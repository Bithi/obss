export class TrBook {
    Id: number;
    book: number;
    requester: number;
    owner: number;
    trDate: Date;
    status: string;

    bimg:   string;
    bookimg:   any;
    bookmockFile:       any;

    uimg:   string;
    userimg:   any;
    usermockFile:       any;

    uId: number;
    user_name:   string;
    nid:   string;
    book_name:   string;
    address:  string;
    phone: string;
}


