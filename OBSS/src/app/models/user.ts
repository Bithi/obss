export class User {
    Id: number;
    userId: string;
    userName: string;
    password: string;
    email: string;
    phone: string;
    nid: string;
    address: string;
    gender: string;
    userStatus: string;
    failCount: string;
    passwordChangeDate: Date;
    createDate: Date;
    updateDate: Date;
    createdBy: number;
    updatedBy: number;
    approvedBy: number;
    img_url:     string;
    image:                 any;
    mockFile:       any;
    education:    string;
    designation: string;
    organization:  string;
    notes:  string;
}


