export class Books {

    id:             number;
    bookName:       string;
    description:    number;
    publishDate:    Date;
    bookCategory:   number;
    category:       string;
    writer:         string;
    bookVersion:    string;
    issn:           string;
    imgUrl:         string;
    image:                 any;
    owner:          number;
    status:         string;
    createDate:     Date;
    updateDate:     Date;
    createdBy:      number;
    updatedBy:      number;
    mockFile:       any;
    ownerName:      string;
}


