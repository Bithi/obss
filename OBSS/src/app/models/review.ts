export class Review {
    id: number;
    user: number;
    review: string;
    rating: number;
    reviewer: number;
    reviewerName: string;
    reviewDate: Date;
    imgUrl: string;
    image: any;
    mockFile: any;

}


