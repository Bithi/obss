export class Menu {
    m_id: number;
    menuName: string;
    parentMenuId: number;
    url: string;
    icon: string;
    component: string;
    module: string;
}


