import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Writer } from 'src/app/models/writer';
import { WriterService } from 'src/app/services/writer.service';
import { SelectItem } from 'primeng/api';
declare var $ : any;
@Component({
  selector: 'app-writer-insert',
  templateUrl: './writer-create.component.html',
  styleUrls: ['./writer-create.component.scss']
})
export class WriterInsertComponent implements OnInit {
  @ViewChild('f') searchForm: NgForm;
  writer: Writer = new Writer();
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  selectedStatus : SelectItem[];
  options: boolean = false;

  constructor(
    private dataService: DataService,
    private service: WriterService,
    public router:Router
  ) { }

  ngOnInit() {
  
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.service.addToTheList.subscribe(
      data =>{
        
      this.writer = data;
      this.options = true;
      }
  );

    this.selectedStatus = [];
    this.selectedStatus.push({ label: 'Select Status', value: -1 });
    this.selectedStatus.push({ label: 'Active', value: 'A' });
    this.selectedStatus.push({ label: 'Inactive', value: 'I' });
  }

  create(): void {       
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.writer)
    }

    this.service.insertCategory(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['writer']);
  });
    
  };


  update(menu): void {

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.writer)
    }
    this.service.updateCategroy(JSON.stringify(this.jsonMessage));
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['writer']);
    });
  };

reset(form) {
    this.writer  = new Writer();
    form.reset({ writerName: this.writer.writerName });
    form.reset({ writerDescription: this.writer.description });
  
}

delete(menu): void {       
  var dataHeader = this.dataService.createMessageHeader("DELETE");
   
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(menu)
  }
  this.service.deleteCategory(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['writer']);
});
 
};

}
