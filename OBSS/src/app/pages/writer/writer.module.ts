import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutModule } from '../layout/layout.module';
import { WriterInsertComponent } from './writer-create/writer-create.component';
import { WriterComponent } from './writer-list/writer-list.component';


@NgModule({
  declarations: [WriterInsertComponent, WriterComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgbModule
  ],
  exports: [WriterInsertComponent,WriterComponent]
})
export class WriterModule { }
