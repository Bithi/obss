import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { Category } from 'src/app/models/category';
import { WriterService } from 'src/app/services/writer.service';
import { Writer } from 'src/app/models/writer';

@Component({
  selector: 'app-writer-info',
  templateUrl: './writer-list.component.html',
  styleUrls: ['./writer-list.component.scss']
})
export class WriterComponent implements OnInit {

  writer: Writer = null;

  listObs: Writer[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  parentMenuPath = new Map();
  constructor(
    private service: WriterService,
    public router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'Id', header: 'ID' },
      { field: 'writerName', header: 'Name' },
      { field: 'description', header: 'Description' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.writer)
    }

    const request = this.service.getList(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.listObs = msg.payLoad;
        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {          
          return;
        }       
      },
      err => {
        
        return;
      }
    );
  }
  editCategory(writer){
    this.service.passTheValue(writer);
  }
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate(['writer']);
    });
  }

}
