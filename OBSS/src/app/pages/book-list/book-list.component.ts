import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Books } from 'src/app/models/books';
import { User } from 'src/app/models/user';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  bookList:Books[];
  selectedCategory:SelectItem[];
  selectedWriter:SelectItem[];
  loginUser:User = new User;
  constructor(private service: ProfileService,public ref: DynamicDialogRef, public config: DynamicDialogConfig) { }

  ngOnInit(): void {
    this.loginUser = JSON.parse(sessionStorage.getItem('loggedinUser'));
    this.getBooksList( this.loginUser.Id);
  }

  labelByValue(labelArray,value){
    if(labelArray){
      let i =  labelArray.find(item => item.value == value);
      return i ? i.label : "";
    }
    
  }
  
  arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  getBooksList(id){

     const request = this.service.getAllBooksDetails(id);
    request.subscribe(
      (res ) => {
              const msg = JSON.parse(res) ;
              this.bookList = msg;
              for(let i = 0; i< msg.length; i++) {
                this.bookList[i].category = this.labelByValue(this.selectedCategory, this.bookList[i].bookCategory);
                this.bookList[i].status = (this.bookList[i].status=="A"?"Active":"Inactive");
                if(this.bookList[i].image){
                  this.bookList[i].image =  this.arrayBufferToBase64(this.bookList[i].image.content);
                }
              
           }

              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
}
selectProduct(product: Books) {
  this.ref.close(product);
}

}
