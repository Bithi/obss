import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApprovalComponent } from './approval.component';
import { LayoutModule } from '../layout/layout.module';
import {ProgressBarModule} from 'primeng/progressbar';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import {MultiSelectModule} from 'primeng/multiselect';
import {SplitButtonModule} from 'primeng/splitbutton';
import {MenuModule} from 'primeng/menu';
import {MenubarModule} from 'primeng/menubar';
@NgModule({
  declarations: [ApprovalComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ProgressBarModule,
    PaginatorModule,
    TableModule,
    FormsModule,
    CalendarModule,
    MultiSelectModule,
    SplitButtonModule,
    MenubarModule

  ]
})
export class ApprovalModule { }
