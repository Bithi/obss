import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { DataService } from 'src/app/data/data.service';
import { Customer, Representative } from 'src/app/models/customer';
import { User } from 'src/app/models/user';
import { CustomerService } from 'src/app/services/customerservice';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.scss']
})
export class ApprovalComponent implements OnInit {

  customers: User[];
  // customer: User = new User;
    selectedCustomers: User[];
    public dtSelectedRows:any[];
    representatives: Representative[];
    jsonMessage:any;
    key:string = "toast";
    severity:string;
    detailMsg:string;
    statuses: any[];
    user: User;
    loading: boolean = true;
    items: MenuItem[];
    @ViewChild('dt') table: Table;

    constructor(private customerService: CustomerService, private utility:Utility, private userService:UserInfoService, private dataService: DataService,     public router: Router) { }

    ngOnInit() {
        this.user = JSON.parse(sessionStorage.getItem('loggedinUser'));
        this.items = [
            {
                label:'',
                icon:'pi pi-cog',
                items:[
                    {
                        label:'Approve',
                        icon:'pi pi-check',
                        command: (data) => { this.approve(this.selectedCustomers) }

                    },
                    {
                        label:'Reject',
                        icon:'pi pi-times',
                        command: (data) => { this.reject(this.selectedCustomers) }
                    },
                    
                ]
            }
        ];
    

        var dataHeader = this.dataService.createMessageHeader("SELECT_APPROVAL_REQUEST_LIST");
        this.jsonMessage = {
            dataHeader : dataHeader,
            payLoad: new Array(this.customers)
        }
      
        const request = this.userService.requestedUsers(JSON.stringify(this.jsonMessage));
        request.subscribe(
            (res ) => {
                    const msg = JSON.parse(atob(res)) ;
                        this.customers = msg.payLoad;
                        this.loading = false;
                    
                
            },
            err => {
                // this.severity = 'error';
                // this.detailMsg = "Server Error: " + err.message;
                // // this.showProgressSpin = false;
                // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );



    
    }

   

    approve(data: User[]) {
        for(let i = 0; i< data.length; i++) {
            data[i].approvedBy =  this.user.Id;
            data[i].userStatus = 'A';
        }
        var dataHeader = this.dataService.createMessageHeader("APPROVE_USERS");
        this.jsonMessage = {
            dataHeader : dataHeader,
            payLoad: data
        }
      
        this.userService.approve(JSON.stringify(this.jsonMessage));
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['userApproval']);
        }); 
    }

    reject(data: User[]) {
        for(let i = 0; i< data.length; i++) {
            data[i].userStatus = 'R';
        }
        var dataHeader = this.dataService.createMessageHeader("REJECT_USERS");
        this.jsonMessage = {
            dataHeader : dataHeader,
            payLoad: data
        }
        this.userService.reject(JSON.stringify(this.jsonMessage));
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['userApproval']);
        }); 
    }
    
}
