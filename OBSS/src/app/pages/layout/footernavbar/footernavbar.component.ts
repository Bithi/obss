import { Component, OnInit } from '@angular/core';
import { Utility } from 'src/app/util/utility';

@Component({
  selector: 'app-footernavbar',
  templateUrl: './footernavbar.component.html',
  styleUrls: ['./footernavbar.component.scss']
})
export class FooternavbarComponent implements OnInit {

  constructor( private utility:Utility) { }

  ngOnInit() {

  }

}
