import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsidenavbarComponent } from './asidenavbar/asidenavbar.component';
import { MessageService } from 'primeng/api';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { FooternavbarComponent } from './footernavbar/footernavbar.component';
import { SettingsnavbarComponent } from './settingsnavbar/settingsnavbar.component';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { DialogModule } from 'primeng/dialog';

import {ButtonModule} from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';
import { TableModule } from 'primeng/table';
@NgModule({
  imports: [
    CommonModule,
    ToastModule,
    RouterModule,
    FormsModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    DynamicDialogModule,
    TableModule,
    FormsModule   
    
  ],
  providers: [MessageService,DialogService],
  declarations: [
      TopnavbarComponent,
      AsidenavbarComponent,
      FooternavbarComponent,
      SettingsnavbarComponent
      
  ],
    exports: [
        TopnavbarComponent,
        AsidenavbarComponent,
        FooternavbarComponent,
        SettingsnavbarComponent
        
    ]
})
export class LayoutModule { }
