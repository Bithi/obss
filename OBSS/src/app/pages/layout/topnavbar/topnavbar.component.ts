import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Router, NavigationEnd } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { ProfileService } from 'src/app/services/profile.service';
import { DataService } from 'src/app/data/data.service';
import { Books } from 'src/app/models/books';
import { RoleWiseUserService } from 'src/app/services/rolewiseuser.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BookListComponent } from '../../book-list/book-list.component';
import { Utility } from 'src/app/util/utility';
declare var $ : any;
@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss']
})
export class TopnavbarComponent implements OnInit, OnDestroy {
  ref: DynamicDialogRef;
  pushRightClass: string = 'push-right';
  user: User;
  selectedBooks:SelectItem[];
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  book: Books;
  books:Books[];
  uId: number;
  selectedCategory:SelectItem[];
  selectedWriter:SelectItem[];
  userMap = new Map();

//   user : any = JSON.parse(sessionStorage.getItem('loggedinUser'));
//   userInfo : User;
  constructor(private utility: Utility, public dialogService: DialogService,public router: Router, private service: ProfileService,private dataService: DataService,  private roleWiseUserService: RoleWiseUserService) {

      this.router.events.subscribe(val => {
          if (
              val instanceof NavigationEnd &&
              window.innerWidth <= 992 &&
              this.isToggled()
          ) {
              this.toggleSidebar();
          }
      });
  }

  ngOnInit() {
    //   console.log(this.user);
    this.user = JSON.parse(sessionStorage.getItem('loggedinUser'));
    // this.getUserList();
    this.selectedBooks = [];
    this.selectedBooks.push({ label: 'Select Books', value: -1});
    // this.getBooksList();
   
  }
  show() {
    this.ref = this.dialogService.open(BookListComponent, {
        header: 'Choose a Product',
        width: '70%',
        contentStyle: {"max-height": "500px", "overflow": "auto"},
        baseZIndex: 10000
    });

    this.ref.onClose.subscribe((product: Books) =>{
        if (product) {
          this.uId = product.owner;
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['userProfile'], { queryParams:  {id:this.uId}});
          });
          
        }
    });
}
ngOnDestroy() {
  if (this.ref) {
      this.ref.close();
  }
}
  isToggled(): boolean {
      const dom: Element = document.querySelector('body');
      return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle(this.pushRightClass);
  }

  rltAndLtr() {
      const dom: any = document.querySelector('body');
      dom.classList.toggle('rtl');
  }

  onLoggedout() {
    sessionStorage.removeItem('isLoggedin');
    sessionStorage.removeItem('loggedinUser');
    sessionStorage.removeItem('roleWiseMenu');
     this.router.navigate(['']);
     return false;
  }
    // alert("hi");
//     getBooksList(){

//       this.selectedBooks=[];
//        const request = this.service.getAllBooksDetails();
//       request.subscribe(
//         (res ) => {
//                 const msg = JSON.parse(res) ;
//                 this.books = msg;
                
//                 for(let i = 0; i< msg.length; i++) {
                  
//                   this.books[i].category = this.labelByValue(this.selectedCategory, this.books[i].bookCategory);
//                   this.books[i].status = (this.books[i].status=="A"?"Active":"Inactive");
//                   if(this.books[i].image){
//                     this.books[i].image =  this.arrayBufferToBase64(this.books[i].image.content);
//                   }
                
//              }
  
                
                
            
//         },
//         err => {
//             // this.severity = 'error';
//             // this.detailMsg = "Server Error: " + err.message;
//             // // this.showProgressSpin = false;
//             // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
//             return;
//         }
//    );
// }
labelByValue(labelArray,value){
  if(labelArray){
    let i =  labelArray.find(item => item.value == value);
    return i ? i.label : "";
  }
  
}

arrayBufferToBase64(buffer) {
  var binary = '';
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

getUserList(){
   
  const request = this.roleWiseUserService.getUserList();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;
            for(let i = 0; i< msg.length; i++) {
              this.userMap.set(msg[i].id,msg[i].userName);
               
         }

            
        
    },
    err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);


}

}