import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { User } from 'src/app/models/user';
 import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';
import { Menu } from 'src/app/models/menu';
import { MenuInfoService } from 'src/app/services/menu-info.service';
import { DataService } from 'src/app/data/data.service';
 declare var require: any;
@Component({
  selector: 'app-asidenavbar',
  templateUrl: './asidenavbar.component.html',
  styleUrls: ['./asidenavbar.component.scss']
})
export class AsidenavbarComponent implements OnInit {
  user: User;
  menu: Menu = null;

  menuInfos: Menu[];
  clicked: boolean = false;
   menus =[];
  
  constructor(public router: Router,  private dataService: DataService,private menuInfoService: MenuInfoService) {

  }
  showMenu: string = '#';
  ngOnInit() {

    this.user = JSON.parse(sessionStorage.getItem('loggedinUser'));
    if(this.user.image){
      this.user.image =  this.arrayBufferToBase64(this.user.image.content);
    }
   
    this.menuInfos = JSON.parse(sessionStorage.getItem('roleWiseMenu'));
    var r = this.convert(this.menuInfos)
    

  }
  // ngAfterViewInit() {
  //   $('[data-widget="treeview"]').each(function() {
  //       AdminLte.Treeview._jQueryInterface.call($(this), 'init');
  //   });
// }

arrayBufferToBase64(buffer) {
  var binary = '';
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}



 convert(list){
  
  var arrayToTree = require('array-to-tree');
  this.menus =arrayToTree(list, {
    parentProperty: 'parentMenuId',
    customID: 'm_id'
  });
  console.log(this.menus );
  }




}


