import { Component, OnInit } from '@angular/core';
import { RoleWiseUser } from 'src/app/models/rolewiseuser';
import { DataService } from 'src/app/data/data.service';
import { RoleWiseUserService } from 'src/app/services/rolewiseuser.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-role-wise-user',
  templateUrl: './role-wise-user.component.html',
  styleUrls: ['./role-wise-user.component.scss']
})
export class RoleWiseUserComponent implements OnInit {
  roleAssigndedToUser: RoleWiseUser = null;

  roleAssigndedToUserInfos: RoleWiseUser[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  constructor(    
    private roleAssignedToUserService: RoleWiseUserService,
    public router: Router,
    private dataService: DataService) {    

  }

  ngOnInit(): void {
    this.cols = [

      { field: 'id', header: 'ID' },
      { field: 'u_id', header: 'User Id' },
      { field: 'r_id', header: 'Role Id' }

    ];
    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.roleAssigndedToUser)
    }

    const request = this.roleAssignedToUserService.showRoleWiseUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.roleAssigndedToUserInfos = msg.payLoad;

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['rolewiseuser']);
  });
  }
  editRoleAssignedToUser(roleAssigndedToUser): void {
    this.roleAssignedToUserService.passTheValue(roleAssigndedToUser);
  }

}
