import { Component, OnInit } from '@angular/core';
import { RoleWiseUser } from 'src/app/models/rolewiseuser';
import { SelectItem } from 'primeng/api/selectitem';
import { DataService } from 'src/app/data/data.service';
import { Utility } from 'src/app/util/utility';
import { Router } from '@angular/router';
import { RoleWiseUserService } from 'src/app/services/rolewiseuser.service';

@Component({
  selector: 'app-role-wise-user-insert',
  templateUrl: './role-wise-user-insert.component.html',
  styleUrls: ['./role-wise-user-insert.component.scss']
})
export class RoleWiseUserInsertComponent implements OnInit {
  options: boolean = false;
  roleWiseUser: RoleWiseUser = new RoleWiseUser();
  selecteduser:SelectItem[];
  selectedrole:SelectItem[];
  jsonMessage:any;
  key:string = "toast";
  severity:string;
  detailMsg:string;
  constructor(private dataService: DataService,
    private roleWiseUserService: RoleWiseUserService,
    private utility:Utility,
    public router:Router) { }

  ngOnInit(): void {

    this.selecteduser = [];
    this.selecteduser.push({ label: 'Select User', value: -1});
    this.getUserList();
   
    this.selectedrole = [];
    this.selectedrole.push({ label: 'Select Role', value: -1});
    this.getTotalRoleList();


    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
      this.roleWiseUserService.addToTheList.subscribe(
      data =>{
        
       this.roleWiseUser = data;
       this.options = false;
      }
    )
    
  }
  getUserList(){
   
    const request = this.roleWiseUserService.getUserList();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selecteduser.push({label: msg[i].userId, value: msg[i].id});
                 
           }

              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );


}
getTotalRoleList(){
   
  const request = this.roleWiseUserService.getMyRoleList();
  request.subscribe(
    (res ) => {
        
            const msg = JSON.parse(res) ;

            for(let i = 0; i< msg.length; i++) {
              this.selectedrole.push({label: msg[i].roleName, value: msg[i].r_id});
             
         }
         console.log( this.selectedrole);
            
        
    },
    err => {
        // this.severity = 'error';
        // this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
        // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);


}

createRoleWiseUser(): void {   
  
  
  if(!this.roleWiseUser.u_id){
    this.severity = 'error';
    this.detailMsg = "User cannot be null";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }

  if(!this.roleWiseUser.r_id){
    this.severity = 'error';
    this.detailMsg = "Role cannot be null";
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    return;
  }

  // this.roleWiseUser.approvedBy=JSON.parse(sessionStorage.getItem('loggedinUser')).Id;
  var dataHeader = this.dataService.createMessageHeader("SAVE");
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(this.roleWiseUser)
  }

  this.roleWiseUserService.insertRoleWiseUser(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['rolewiseuser']);
});
  
};





reset(form) {

 
    this.roleWiseUser  = new RoleWiseUser();
    form.reset({ m_id: this.roleWiseUser.u_id });
    form.reset({ r_id: this.roleWiseUser.r_id });
    form.reset({ options: false });
  }
  



updateRoleWiseUser(roleWiseUser): void {
  var dataHeader = this.dataService.createMessageHeader("UPDATE");

  this.jsonMessage = {
    dataHeader: dataHeader,
    payLoad: new Array(this.roleWiseUser)
  }
  //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
  this.roleWiseUserService.updateRoleWiseUser(JSON.stringify(this.jsonMessage));

  //   request.subscribe(
  //     (data ) => {
  //       const msg = JSON.parse( atob(data) );
  //      // alert("Employee created successfully.");
  //      debugger;
  //      console.log(msg.payload);
  //     });

  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['rolewiseuser']);
});
};


deleteRoleWiseUser(roleWiseUser): void {    

  var dataHeader = this.dataService.createMessageHeader("DELETE");
  
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(roleWiseUser)
  }
  this.roleWiseUserService.deleteRoleWiseUser(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['rolewiseuser']);
});
 
};
}
