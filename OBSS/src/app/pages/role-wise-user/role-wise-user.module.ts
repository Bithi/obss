import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleWiseUserComponent } from './role-wise-user/role-wise-user.component';
import { RoleWiseUserInsertComponent } from './role-wise-user-insert/role-wise-user-insert.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../layout/layout.module';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  declarations: [RoleWiseUserComponent, RoleWiseUserInsertComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule
  ]
})
export class RoleWiseUserModule { }
