import { Component, ElementRef, OnInit } from '@angular/core';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { Books } from 'src/app/models/books';
import { Category } from 'src/app/models/category';
import { Utility } from 'src/app/util/utility';
import { DataService } from 'src/app/data/data.service';
import { ProfileService } from 'src/app/services/profile.service';
import {  Router } from '@angular/router';
import { Writer } from 'src/app/models/writer';
import { Writerbook } from 'src/app/models/writerbook';
import { User } from 'src/app/models/user';
import { LoginService } from '../../services/login.service';
import { UserInfoService } from 'src/app/services/user-info.service';
import { Review } from 'src/app/models/review';
import { ActivatedRoute } from '@angular/router';
import { TrBook } from 'src/app/models/trbook';
declare var $ : any;
declare var jQuery:any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [ConfirmationService]
})
export class ProfileComponent implements OnInit {

  total:number;
  totalRev:number;
  totalReq:number;
  totalToBeRetBooks:number;
  totalToClaimBooks:number;
  openAddBook : boolean = false;
  selectedStatus : SelectItem[];
  selectedCategory:SelectItem[];
  selectedWriter:SelectItem[];
  book : Books = new Books();
  review : Review = new Review();
  categories: Category[];
  key:string = "toast";
  severity:string;
  detailMsg:string;
  jsonMessage:any;
  currentFileUpload: File;
  user: User= new User;
  updatedUser: User = new User;
  books: Books[];
  gaveReview:Review = new Review;
  reviews:Review[];
  reqBooks: TrBook[];
  toBeRetBooks: TrBook[];
  toClaimBooks:TrBook[];
  element: HTMLImageElement;
  loginUser:User = new User;
  trBook: TrBook = new TrBook();
  public show:boolean;
  public req:boolean;
  public rev:boolean;
  requester:number;
  bookStatus = new Map();
  currentRate = 0;
  showUserId:number;
  constructor(private utility:Utility,
    private dataService: DataService,
    private service: ProfileService,
    public router:Router,
    private confirmationService : ConfirmationService,
    private userInfoService: UserInfoService,
    private loginService: LoginService,
    private aroute: ActivatedRoute) { }
  ngOnInit(): void {
    // $('.container').rating();
    this.rev=false;
    this.req = false;
    this.show = false;
    this.loginUser = JSON.parse(sessionStorage.getItem('loggedinUser')); 

    this.aroute.queryParams
      .subscribe(params => {
      if(!$.isEmptyObject(params))
      { 
        this.showUserId= params.id;
        this.show = true;
        this.rev=true;
        this.getUserDtl(params.id);
        this.getBooksList(params.id);
        this.getReviewList(params.id);
        
       
      }
      else{
        this.show = false;
        this.rev=false;
        this.user =this.loginUser;
        this.getUserDtl(this.user.Id);
        this.getBooksList(this.user.Id);
        this.getReviewList(this.user.Id);
        this.requestedBookList(this.user.Id);
        this.toBeRetBookList(this.user.Id);
        this.getToClaimBooks(this.user.Id);
      
      }
      
    }
      
    );
   
   
    
    this.selectedStatus = [];
    this.selectedStatus.push({ label: 'Select Status', value: -1 });
    this.selectedStatus.push({ label: 'Active', value: 'A' });
    this.selectedStatus.push({ label: 'Inactive', value: 'I' });

    //fetch category
    this.selectedCategory = [];
    this.selectedCategory.push({ label: 'Select Category', value: -1});
    this.getCategoryList();
    //fetch writers
    this.selectedWriter = [];
    this.selectedWriter.push({ label: 'Select Writer', value: -1});
    this.getWriterList();


    //get user book list

  
  }
  getUserDtl(u:number)
  {
  
    const request = this.service.getUserDetails(u);
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
             
              this.user = msg;
              if(this.user.image){
                this.user.image =  this.arrayBufferToBase64(this.user.image.content);
              }
      },
      err => {
        return;
      });
  
    
    
  }
  getCategoryList(){
    const request = this.service.getCategoryList();
    request.subscribe(
      (res ) => {
          
              const msg = JSON.parse(res) ;
              for(let i = 0; i< msg.length; i++) {
                this.selectedCategory.push({label: msg[i].name, value: msg[i].id});                 
           }
      },
      err => {
        return;
      });
  }
  getWriterList(){
    const request = this.service.getWriterList();
    request.subscribe(
      (res ) => {
          
          const msg = JSON.parse(res) ;
          
          for(let i = 0; i< msg.length; i++) {
            this.selectedWriter.push({label: msg[i].writerName, value: msg[i].id});                 
          }
      },
      err => {
        return;
      });
  }
  getReviewList(id){
  
    const request = this.service.getReviewist(id);
    request.subscribe(
      (res ) => {
        this.reviews = JSON.parse(res);
          this.totalRev=this.reviews.length;
          for(var i=0;i<this.reviews.length;i++){
            if(this.reviews[i].image){
              this.reviews[i].image =  this.arrayBufferToBase64(this.reviews[i].image.content);
            }
          }

       
          
      },
      err => {
        return;
      });
  }
  getBooksList(id){
    const request = this.service.getBooksList(id);
    request.subscribe(
      (res ) => {
          this.books = JSON.parse(res);
          this.total=this.books.length;
          for(var i=0;i<this.books.length;i++){
            this.books[i].category = this.labelByValue(this.selectedCategory, this.books[i].bookCategory);
            this.books[i].status = (this.books[i].status=="A"?"Active":"Inactive");
            if(this.books[i].image){
              this.books[i].image =  this.arrayBufferToBase64(this.books[i].image.content);
            }
              this.showReqBookStatus( this.books[i].id);
           
          }
          
      },
      err => {
        return;
      });
      
  }
  requestedBookList(id){
    const request = this.service.requestedBookList(id);
    request.subscribe(
      (res ) => {
        debugger;
          this.reqBooks = JSON.parse(res);
          this.totalReq=this.reqBooks.length;
          for(var i=0;i<this.totalReq;i++){
           
            if(this.reqBooks[i].bimg){
              this.reqBooks[i].bookimg =  this.arrayBufferToBase64(this.reqBooks[i].bookimg.content);
            }
              
            if(this.reqBooks[i].uimg){
              this.reqBooks[i].userimg =  this.arrayBufferToBase64(this.reqBooks[i].userimg.content);
            }
          }
          
      },
      err => {
        return;
      });
      
  }

  toBeRetBookList(id){
    const request = this.service.toBeReturnBookList(id);
    request.subscribe(
      (res ) => {
          this.toBeRetBooks = JSON.parse(res);
          this.totalToBeRetBooks=this.toBeRetBooks.length;
          for(var i=0;i<this.totalToBeRetBooks;i++){
           
            if(this.toBeRetBooks[i].bimg){
              this.toBeRetBooks[i].bookimg =  this.arrayBufferToBase64(this.toBeRetBooks[i].bookimg.content);
            }
              
           
          }
          
      },
      err => {
        return;
      });
      
  }
  
  getToClaimBooks(id){
    const request = this.service.getToClaimBooks(id);
    request.subscribe(
      (res ) => {
          this.toClaimBooks = JSON.parse(res);
          this.totalToClaimBooks=this.toClaimBooks.length;
          for(var i=0;i<this.totalToClaimBooks;i++){
           
            if(this.toClaimBooks[i].bimg){
              this.toClaimBooks[i].bookimg =  this.arrayBufferToBase64(this.toClaimBooks[i].bookimg.content);
            }
              
           
          }
          
      },
      err => {
        return;
      });
      
  }
  openBookModal(){
    this.openAddBook = true;
  }
  
  showReqBookStatus(book){
   
    this.requester  = this.loginUser.Id;
    
    const request = this.service.showReqBookStatus(this.requester,book);
    request.subscribe(
      (res ) => {
        debugger;
        if(res==='request'){
          this.bookStatus.set(book, 1);
        }
        else  if(res==='approved'){
          this.bookStatus.set(book, 2);
        }
        else  if(res==='rejected'){
          this.bookStatus.set(book, 3);
        }
        else if (res==='returned'){
          this.bookStatus.set(book, 5);
        }
        else if (res===''){
          this.bookStatus.set(book, 4);
        }
          
      },
      err => {
        return;
      });
      
  }
  saveBook(): void {

    if(!this.book.bookName){
      this.severity = 'error';
      this.detailMsg = "Book Name cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }

    if(!this.book.status){
      this.severity = 'error';
      this.detailMsg = "Status cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.book.bookCategory){
      this.severity = 'error';
      this.detailMsg = "Category cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(!this.book.writer){
      this.severity = 'error';
      this.detailMsg = "Writer cannot be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
    }
    if(this.currentFileUpload ){
      this.book.imgUrl = this.currentFileUpload.name;
    }
    
    this.book.image =null;
    this.book.owner = JSON.parse(sessionStorage.getItem('loggedinUser')).Id;
    this.book.createdBy = JSON.parse(sessionStorage.getItem('loggedinUser')).Id;
   
    const formData: FormData = new FormData();
    formData.append('file', this.currentFileUpload);
    formData.append('book', JSON.stringify(this.book));
    const writerbook = new Writerbook();
    writerbook.writer = parseInt(this.book.writer.toString());
    formData.append('writerBook',JSON.stringify(writerbook));

    this.service.saveBook(formData);

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
      
    });
    
  };
  selectFile(event) {  
    this.currentFileUpload = event.target.files.item(0);
    $('.custom-file-label').html(this.currentFileUpload.name);
    document.getElementById('imageShow').setAttribute('src',URL.createObjectURL(event.target.files[0]));
   

  }
  arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  labelByValue(labelArray,value){
    if(labelArray){
      let i =  labelArray.find(item => item.value == value);
      return i ? i.label : "";
    }
    
  }
  delete(book) {
    book.image=null;
    book.createDate = null;
    book.updateDate = null;
    book.publishDate = null;
    this.confirmationService.confirm({
        message: 'Do you want to delete this record?',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          //send delete request
          var dataHeader = this.dataService.createMessageHeader("DELETE");
          this.jsonMessage = {
              dataHeader : dataHeader,
              payLoad: new Array(book)
          }
      
          this.service.deleteBook(JSON.stringify(this.jsonMessage));
      
          this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
            this.router.navigate(['userProfile']);
          });
          this.severity = 'info';
          this.detailMsg = "Record deleted";
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        },
        reject: () => {
          this.severity = 'info';
          this.detailMsg = "You have rejected";
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        }
    });
  }
  update(book) {
    const request = this.service.getBooksDetails(book.id);
    request.subscribe(
      (res ) => {
        var response = JSON.parse(res)[0];
         this.book.id = response[0];
         this.book.bookName = response[1];
         this.book.description = response[2];
         this.book.publishDate = response[3];
         this.book.bookCategory = response[4];
         this.book.bookVersion = response[5];
         this.book.issn = response[6];
         this.book.imgUrl = response[7]; 
         //this.book.image = response[8];
         this.book.owner = response[8];
         this.book.status = response[9];
         this.book.createDate = response[10]; 
         this.book.updateDate = response[11];
         this.book.createdBy = response[12];
         this.book.updatedBy = response[13];
         this.book.category = response[14];
         this.book.writer = response[15];

         this.book.image = book.image;
         this.openAddBook = true;
        //  var dataHeader = this.dataService.createMessageHeader("DELETE");
        //   this.jsonMessage = {
        //       dataHeader : dataHeader,
        //       payLoad: new Array(book)
        //   }
      
        //   this.service.deleteBook(JSON.stringify(this.jsonMessage));

        //   this.saveBook();
        
         
          
      },
      err => {
        return;
      });
    //this.book = book;
   
  }

  updateUser(): void {

    this.updatedUser.passwordChangeDate = new Date(this.updatedUser.passwordChangeDate);
    this.updatedUser.updateDate =  new Date(this.updatedUser.updateDate);
    this.updatedUser.createDate =  new Date( this.updatedUser.createDate);
    debugger;
    if(this.currentFileUpload){
      this.updatedUser.img_url = this.currentFileUpload.name;
      const formData: FormData = new FormData();
    formData.append('file', this.currentFileUpload);
    formData.append('user', JSON.stringify(this.updatedUser));
    this.service.updateUser(formData);
    }
    else{
      var dataHeader = this.dataService.createMessageHeader("UPDATE");
      this.updatedUser.image = null;
      this.jsonMessage = {
          dataHeader: dataHeader,
          payLoad: new Array(this.updatedUser)
      }
      //const request = this.userInfoService.insertUserInfo(JSON.stringify(this.jsonMessage));
      this.userInfoService.updateUserInfo(JSON.stringify(this.jsonMessage));
    }

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
    
  };
  
  request(book): void {
    this.trBook = new TrBook();
    this.trBook.book=book.id;
    this.trBook.owner=book.owner;
    this.trBook.requester = this.loginUser.Id;
    this.trBook.status="request";
    
    var dataHeader = this.dataService.createMessageHeader("REQUEST");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array( this.trBook)
    }
  

    this.service.requestBook(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
    
  };
  claim(book):void{
    this.trBook = new TrBook();
    this.trBook.Id=book.Id;
    this.trBook.book=book.book;
    this.trBook.owner=book.owner;
    this.trBook.requester = book.requester;
    this.trBook.status="claimed";
    var dataHeader = this.dataService.createMessageHeader("CLAIM");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array( this.trBook)
    }
  

    this.service.claimBook(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
  }
  return(book): void {
    debugger;
    this.trBook = new TrBook();
    this.trBook.Id=book.Id;
    this.trBook.book=book.book;
    this.trBook.owner=book.owner;
    this.trBook.requester = book.requester;
    this.trBook.status="returned";
    
    var dataHeader = this.dataService.createMessageHeader("RETURN");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array( this.trBook)
    }
  

    this.service.returnBook(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
    
  };
  approve(book): void {
    debugger;
    this.trBook = new TrBook();
    this.trBook.Id=book.Id;
    this.trBook.book=book.book;
    this.trBook.owner=book.owner;
    this.trBook.requester = book.requester;
    this.trBook.status="approved";
    
    var dataHeader = this.dataService.createMessageHeader("APPROVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array( this.trBook)
    }
  

    this.service.approveBook(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
    
  };

  reject(book): void {
    this.trBook = new TrBook();
    this.trBook.Id=book.Id;
    this.trBook.book=book.book;
    this.trBook.owner=book.owner;
    this.trBook.requester = book.requester;
    this.trBook.status="rejected";
    
    var dataHeader = this.dataService.createMessageHeader("REJECT");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array( this.trBook)
    }
  

    this.service.rejectBook(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['userProfile']);
    });
    
  };

  editUser(user:User): void{
    this.updatedUser = user;
  };
  onPageChange(event) {
    this.reviews = this.reviews.slice(event.first, event.first+event.rows);
 }
 showUser(userId){

 this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
  this.router.navigate(['userProfile'], { queryParams:  {id:userId}});
});
};

givereview(rev:Review): void {
  this.gaveReview = new Review();
  this.gaveReview.user=this.showUserId;
  this.gaveReview.review=rev.review;
  this.gaveReview.rating=rev.rating;
  this.gaveReview.reviewer=this.loginUser.Id;
  
  var dataHeader = this.dataService.createMessageHeader("SAVE");
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array( this.gaveReview)
  }


  this.service.giveReview(JSON.stringify(this.jsonMessage));

  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['userProfile']);
  });
  
};

}
