import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { LayoutModule } from '../layout/layout.module';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {DataViewModule} from 'primeng/dataview';
import {PanelModule} from 'primeng/panel';

import {CardModule} from 'primeng/card';
import {PaginatorModule} from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import {ConfirmDialogModule} from 'primeng/confirmdialog';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    LayoutModule,
    DialogModule,
    DropdownModule,
    NgbModule,
    FormsModule,
    DataViewModule,
    PanelModule,
    CardModule,
    PaginatorModule,
    TableModule,
    ConfirmDialogModule
    
  ]
})
export class ProfileModule { }
