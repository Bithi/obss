import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import {MessageService} from 'primeng/api';
import { LoginService } from '../../services/login.service';
import { DataService } from '../../data/data.service';
import { Utility } from '../../util/utility';
import { User } from '../../models/user';
import { UserInfoService } from 'src/app/services/user-info.service';
declare var $ : any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  constructor(
    public router: Router,
    private loginService: LoginService,
    private dataService: DataService,
    private http: HttpClient,
    private messageService: MessageService,
    private utility:Utility,
    private userService:UserInfoService
    ) 
    { 

    }
    displayMaximizable: boolean;
    User: User = new User();
    registerUser: User = new User();
    key:string = "toast";
    severity:string;
    detailMsg:string;
    // showProgressSpin:boolean;
    loadingMessage:string = "Loading...";
  ngOnInit() {
     document.body.className = 'container';
        $('.js-tilt').tilt({
      scale: 1.1
  })
    
  }
  showMaximizableDialog() {
    this.registerUser = new User;
    this.displayMaximizable = true;
}

  onLoggedin(){
    if(!this.User.userId||!this.User.password){
      this.severity = 'error';
      this.detailMsg = (this.User.userId?"Password":"Username")+" should not be empty";
      this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
  }
//   this.showProgressSpin = true;

  var dataHeader = this.dataService.createMessageHeader("USER_LOGIN");
  var jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(this.User)
  }

  this.loginService.getUserDetails(JSON.stringify(jsonMessage));

//   

  }

  userNameEmptyCheck(){

    if(!this.User.userId){
        this.detailMsg = "Username should not be empty";
        this.severity = 'error';
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }
    

}
passwordEmptyCheck(){
    if(!this.User.password){
        this.detailMsg = "Password should not be empty";
        this.severity = 'error';
        this.utility.ShowToast(this.key,this.severity,this.detailMsg);
    }

}

register(user:User){
  if(!user.userId){
    this.detailMsg = "UserId should not be empty";
    this.severity = 'error';
    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}
if(!user.userName){
  this.detailMsg = "userName should not be empty";
  this.severity = 'error';
  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}
if(!user.password){
  this.detailMsg = "password should not be empty";
  this.severity = 'error';
  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}
if(!user.email){
  this.detailMsg = "email should not be empty";
  this.severity = 'error';
  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}
if(!user.phone){
  this.detailMsg = "Phone should not be empty";
  this.severity = 'error';
  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}
if(!user.nid){
  this.detailMsg = "NID should not be empty";
  this.severity = 'error';
  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
}

var dataHeader = this.dataService.createMessageHeader("USER_REGISTER");
var jsonMessage = {
    dataHeader : dataHeader,
    payLoad: new Array(user)
}

this.userService.insertUserInfo(JSON.stringify(jsonMessage));

  this.displayMaximizable = false;
  
  this.severity = 'success';
  this.detailMsg = "Registration completed. plz wait for approval.";
  this.utility.ShowToast(this.key, this.severity, this.detailMsg);
}

}