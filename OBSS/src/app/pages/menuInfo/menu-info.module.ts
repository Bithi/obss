import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuInfoInsertComponent } from './menu-info-insert/menu-info-insert.component';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { MenuInfoComponent } from './menu-info/menu-info.component';
import { LayoutModule } from '../layout/layout.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [MenuInfoInsertComponent, MenuInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgbModule
  ],
  exports: [MenuInfoInsertComponent,MenuInfoComponent]
})
export class MenuInfoModule { }
