import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/models/menu';
import { MenuInfoService } from 'src/app/services/menu-info.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { DropdownInfoService } from 'src/app/services/dropdown-info.service';

@Component({
  selector: 'app-menu-info',
  templateUrl: './menu-info.component.html',
  styleUrls: ['./menu-info.component.scss']
})
export class MenuInfoComponent implements OnInit {

  menu: Menu = null;

  menuInfos: Menu[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  parentMenuPath = new Map();
  constructor(
    private menuInfoService: MenuInfoService,
    public router: Router,
    private dataService: DataService,
    private dropdownInfoService: DropdownInfoService
  ) { }

  ngOnInit(): void {
    this.getParentMenuList();
    this.cols = [

      { field: 'menuName', header: 'Menu' },
      { field: 'parentMenuId', header: 'Parent Menu' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.menu)
    }

    const request = this.menuInfoService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.menuInfos = msg.payLoad;
        console.log(this.menuInfos);

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
  }
  getParentMenuList(){
   
    const request = this.dropdownInfoService.getMenuList();
    request.subscribe(
      (res ) => {
              const msg = JSON.parse(res) ;
              const menu =msg;
              var parentMenuName = "";
              for(let i = 0; i< msg.length; i++) {
                
                // var parentId = msg[i].parentMenuId;
                // for(let k = 0; k< menu.length; k++) {
                //   if(menu[k].m_id === parentId){
                //     parentMenuName = '('+ menu[k].menuName+')';
                //     if(parentMenuName ==="(root)"){
                //       parentMenuName = ""
                //     }
                    
                //   }
                // }
                this.parentMenuPath.set(msg[i][0], msg[i][2].substring(6));
                // this.selectedmenu.push({label: msg[i].menuName, value: msg[i].m_id});
                // console.log( this.vendorList);
           }


              
              
          
      },
      err => {
          // this.severity = 'error';
          // this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
          // this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
 
}
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['menuinfo']);
  });
  }

  editMenu(menu): void {
    this.menuInfoService.passTheValue(menu);
  }

}
