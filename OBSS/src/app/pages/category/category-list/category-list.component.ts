import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';
import { CategoryService } from 'src/app/services/category.service';
import { Category } from 'src/app/models/category';

@Component({
  selector: 'app-category-info',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryComponent implements OnInit {

  category: Category = null;

  listObs: Category[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };
  parentMenuPath = new Map();
  constructor(
    private service: CategoryService,
    public router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.cols = [

      { field: 'id', header: 'ID' },
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.category)
    }

    const request = this.service.getList(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.listObs = msg.payLoad;
        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {          
          return;
        }       
      },
      err => {
        
        return;
      }
    );
  }
  editCategory(category){
    this.service.passTheValue(category);
  }
  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
        this.router.navigate(['category']);
    });
  }

}
