import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoryComponent } from './category-list/category-list.component';
import { LayoutModule } from '../layout/layout.module';
import { CategoryInsertComponent } from './category-create/category-create.component';



@NgModule({
  declarations: [CategoryInsertComponent, CategoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgbModule
  ],
  exports: [CategoryInsertComponent,CategoryComponent]
})
export class CategoryModule { }
