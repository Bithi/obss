import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
declare var $ : any;
@Component({
  selector: 'app-menu-info-insert',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss']
})
export class CategoryInsertComponent implements OnInit {
  @ViewChild('f') searchForm: NgForm;
  options: boolean = false;
  category: Category = new Category();
  jsonMessage:any;
  key:string = "toast";
    severity:string;
    detailMsg:string;

  constructor(
    private dataService: DataService,
    private service: CategoryService,
    public router:Router
  ) { }

  ngOnInit() {
  
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.service.addToTheList.subscribe(
        data =>{
          
        this.category = data;
        this.options = false;
        }
    );
  }

  create(): void {       
      
    var dataHeader = this.dataService.createMessageHeader("SAVE");
    this.jsonMessage = {
        dataHeader : dataHeader,
        payLoad: new Array(this.category)
    }

    this.service.insertCategory(JSON.stringify(this.jsonMessage));

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['category']);
  });
    
  };


  update(menu): void {

    var dataHeader = this.dataService.createMessageHeader("UPDATE");

    this.jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.category)
    }
    this.service.updateCategroy(JSON.stringify(this.jsonMessage));
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['category']);
    });
  };

reset(form) {
    this.category  = new Category();
    form.reset({ menuName: this.category.name });
    form.reset({ menuDescription: this.category.description });
  
}

delete(menu): void {       
  var dataHeader = this.dataService.createMessageHeader("DELETE");
   
  this.jsonMessage = {
      dataHeader : dataHeader,
      payLoad: new Array(menu)
  }
  this.service.deleteCategory(JSON.stringify(this.jsonMessage));
  this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
    this.router.navigate(['category']);
});
 
};

}
