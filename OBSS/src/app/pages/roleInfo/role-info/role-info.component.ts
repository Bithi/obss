import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/role';
import { RoleInfoService } from 'src/app/services/role-info.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data/data.service';

@Component({
  selector: 'app-role-info',
  templateUrl: './role-info.component.html',
  styleUrls: ['./role-info.component.scss']
})
export class RoleInfoComponent implements OnInit {

  role: Role = null;

  roleInfos: Role[];
  cols: any[];
  jsonMessage: { dataHeader: any; payLoad: any; };

  constructor(
    private roleInfoService: RoleInfoService,
    public router: Router,
    private dataService: DataService,
  ) { }

  reloadPage(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['roleinfo']);
  });
  }

  ngOnInit() {
    this.cols = [

      { field: 'r_id', header: 'Role Id' },
      { field: 'roleName', header: 'Role' },
      { field: 'status', header: 'Status' }

    ];

    var dataHeader = this.dataService.createMessageHeader("SELECT_ALL");

    var jsonMessage = {
      dataHeader: dataHeader,
      payLoad: new Array(this.role)
    }

    const request = this.roleInfoService.showUserInfo(JSON.stringify(jsonMessage));
    request.subscribe(
      (res) => {

        const msg = JSON.parse(atob(res));
        console.log(msg);
        this.roleInfos = msg.payLoad;
        console.log(this.roleInfos);

        if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {


          
          return;
        }
       



      },
      err => {
        
        return;
      }
    );
    
  }

  editRole(role): void {
    this.roleInfoService.passTheValue(role);
  }

}
