import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleInfoInsertComponent } from './role-info-insert/role-info-insert.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { RoleInfoComponent } from './role-info/role-info.component';
import { LayoutModule } from '../layout/layout.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [RoleInfoInsertComponent, RoleInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgbModule
  ],
  exports: [RoleInfoInsertComponent,RoleInfoComponent]
})
export class RoleInfoModule { }
