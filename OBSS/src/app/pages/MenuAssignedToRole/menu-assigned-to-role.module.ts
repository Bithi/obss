import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuAssignedToRoleInsertComponent } from './menu-assigned-to-role-insert/menu-assigned-to-role-insert.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { MenuAssignedToRoleComponent } from './menu-assigned-to-role/menu-assigned-to-role.component';
import { KeyFilterModule } from 'primeng/keyfilter';
import {FieldsetModule} from 'primeng/fieldset';
import { LayoutModule } from '../layout/layout.module';



@NgModule({
  declarations: [MenuAssignedToRoleInsertComponent, MenuAssignedToRoleComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,KeyFilterModule,FieldsetModule,
    DropdownModule,
    ProgressSpinnerModule
  ],
  exports: [MenuAssignedToRoleInsertComponent]
})
export class MenuAssignedToRoleModule { }
