import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserInfoComponent } from './user-info/user-info.component';
import { UserInfoRoutingModule } from './user-info/user-info-routing.module';
import { UserInfoInsertComponent } from './user-info-insert/user-info-insert.component';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { UserInfoService } from 'src/app/services/user-info.service';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutModule } from '../layout/layout.module';


@NgModule({
  declarations: [UserInfoComponent, UserInfoInsertComponent],
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    TableModule,
    DropdownModule,
    ProgressSpinnerModule,
    NgbModule
  ],
  providers: [UserInfoService],
  exports: [UserInfoComponent,UserInfoInsertComponent]
})
export class UserInfoModule { }
