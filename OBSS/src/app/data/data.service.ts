import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';

import {DataHeader} from './data.header';

/*const headers = new HttpHeaders({ 'Content-Type': 'application/json'});
*/

//const headers =new HttpHeaders().set("Authorization","Bearer "+sessionStorage.getItem('loggedinUser'));

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) {

  }

  createMessageHeader(actionType: string) {
    var dataHeader = new DataHeader();
    dataHeader.requestTimestamp = this.getSqlDate(new Date().toString());
    dataHeader.actionType = actionType;
    return dataHeader;

  }

  createRequest(reqUrl, jsonMessage) {
    return this.http.post(reqUrl,
      btoa(JSON.stringify(jsonMessage)),
      {responseType: 'text'}
    );

  }

  createRequestArrayBuffer(reqUrl, jsonMessage) {
    return this.http.post(reqUrl,
      btoa(JSON.stringify(jsonMessage)),
      {responseType: 'arraybuffer'}
    );

  }

  randomString(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

    if (!length) {
      length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
      str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
  }

  getSqlDate(stringDate) {
    if (!stringDate) {
      return stringDate;
    }
    return new Date(stringDate).toISOString();
  }
}
