import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { LoginComponent } from './pages/login/login.component';
import { LayoutModule } from './pages/layout/layout.module';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { TableModule } from 'primeng/table';
import { MenuInfoModule } from './pages/menuInfo/menu-info.module';
import { RoleInfoModule } from './pages/roleInfo/role-info.module';
import { MenuAssignedToRoleModule } from './pages/MenuAssignedToRole/menu-assigned-to-role.module';
import { ProfileModule } from './pages/profile/profile.module';
import { RoleWiseUserModule } from './pages/role-wise-user/role-wise-user.module';
import { ApprovalModule } from './pages/approval/approval.module';
import { CustomerService } from './services/customerservice';
import { CategoryModule } from './pages/category/category.module';
import { WriterModule } from './pages/writer/writer.module';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { BookListComponent } from './pages/book-list/book-list.component';
import { UserInfoModule } from './pages/user-info/user-info.module';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BookListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ToastModule,
    DialogModule,
    BrowserAnimationsModule,
    FormsModule,
    LayoutModule,
    DashboardModule,
    MenuInfoModule,
    RoleInfoModule,
    MenuAssignedToRoleModule,
    ProfileModule,
    RoleWiseUserModule,
    ApprovalModule,
    UserInfoModule,
    CategoryModule,
    WriterModule,
    OverlayPanelModule,
    TableModule   

  ],
  providers: [MessageService,CustomerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
