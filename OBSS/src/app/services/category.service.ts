import {
  Injectable
} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Router} from '@angular/router';
import { Utility} from '../util/utility';
import {  DataService} from '../data/data.service';
import {  GlobalVariable} from '../global/global';
import { Subject,  Observable} from 'rxjs';
import { Category } from '../models/category';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  public addToTheList = new Subject<any>();
    public category:Category = new Category();
    private baseUrl = GlobalVariable.BASE_API_URL + '/category';

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}


      getList(jsonMessage) {
        return this.http.post(this.baseUrl,btoa(jsonMessage), { responseType: 'text'}
        );
      }


      public insertCategory(jsonMessage) {
        return this.http.post(GlobalVariable.BASE_API_URL + '/category',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    sessionStorage.removeItem('isLoggedin');
                    // // this.showProgressSpin = false;
                    return;
                }
                // this.showProgressSpin = false;
                //sessionStorage.setItem('isLoggedin', 'true');
                //sessionStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
                // this.router.navigate(['/dashboard']);
                this.severity = 'success';
                this.detailMsg = "Category inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }


    public showCategory(jsonMessage){
      return this.http.post(GlobalVariable.BASE_API_URL+'/insertRoleInfo',
         btoa(jsonMessage)
     , { responseType: 'text'}
         )
   }


   public updateCategroy(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/category',
    btoa(jsonMessage)
, { responseType: 'text'}
    )
    .subscribe(
      (res ) => {
              const msg = JSON.parse( atob(res) );
              console.log(res);
              
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                   this.severity = 'error';
                   this.detailMsg = "Error occured";
                   this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                   sessionStorage.removeItem('isLoggedin');
                  // this.showProgressSpin = false;
                  return;
              }
              // this.showProgressSpin = false;
              this.severity = 'success';
              this.detailMsg = "Category updated Successfully";
              this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              
              
          
      },
      err => {
           this.severity = 'error';
           this.detailMsg = "Server Error: " + err.message;
          // // this.showProgressSpin = false;
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
  }


  public deleteCategory(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/category',
    btoa(jsonMessage)
    , { responseType: 'text'}
    )
    .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 sessionStorage.removeItem('isLoggedin');
                //this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
           this.severity = 'success';
           this.detailMsg = "Deleted Successfully";
           this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
         // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
    );
    }


  passTheValue(categoryData){
    var category = JSON.parse(JSON.stringify(categoryData));
    this.addToTheList.next(category);
  }
  

}