import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global/global';
import { Router } from '@angular/router';
import {MessageService} from 'primeng/api';
import{Utility} from '../util/utility';
import { Menu } from '../models/menu';
import { MenuInfoService } from './menu-info.service';
import { DataService } from '../data/data.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http:HttpClient,
    public router: Router,
    private messageService: MessageService,
    private menuInfoService: MenuInfoService,
    private dataService: DataService,
    private utility:Utility) 
    { }
  
    key:string = "toast";
    severity:string;
    detailMsg:string;
    menu: Menu = null;
    user: User;
    menuInfos: Menu[];
    clicked: boolean = false;
     menus =[];


  getUserDetails(jsonMessage){
    this.http.post(GlobalVariable.BASE_API_URL+'/login',
      btoa(jsonMessage)
  , { responseType: 'text'}
      )
  .subscribe(
      (res ) => {
          
              const msg = JSON.parse( atob(res) );
              if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                  this.severity = 'error';
                  this.detailMsg = "Invalid Credentials";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  sessionStorage.removeItem('isLoggedin');
                  // this.showProgressSpin = false;
                  return;
              }
              sessionStorage.setItem('isLoggedin', 'true');
              sessionStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
              this.getMenus();
              this.router.navigate(['/dashboard']);
              
              
          
      },
      err => {
          this.severity = 'error';
          this.detailMsg = "Server Error: " + err.message;
          // this.showProgressSpin = false;
          this.utility.ShowToast(this.key,this.severity,this.detailMsg);
          return;
      }
 );
    }


    getMenus(){
      var dataHeader = this.dataService.createMessageHeader("SHOW_MENU");
      this.user = JSON.parse(sessionStorage.getItem('loggedinUser'));
        var jsonMessage = {
          dataHeader: dataHeader,
          payLoad: new Array(this.user)
        }
    
        const request = this.menuInfoService.showMenuByUser(JSON.stringify(jsonMessage));
        request.subscribe(
          (res) => {
            const msg = JSON.parse(atob(res));
            // this.menuInfos = msg.payLoad;
            sessionStorage.setItem('roleWiseMenu', JSON.stringify(msg.payLoad));
            // var r = this.convert(this.menuInfos)
            if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]) {
    
    
              
              return;
            }
           
    
    
    
          },
          err => {
            
            return;
          }
        );
    };
    
    
    //  convert(list){
      
    //   var arrayToTree = require('array-to-tree');
    //   this.menus =arrayToTree(list, {
    //     parentProperty: 'parentMenuId',
    //     customID: 'm_id'
    //   });
    //   console.log(this.menus );
    //   }

  }

