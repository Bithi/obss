import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalVariable } from '../global/global';
import { User } from '../models/user';

@Injectable()
export class CustomerService {

    constructor(private http: HttpClient) { }

    getCustomersSmall() {
        return this.http.get<any>('assets/showcase/data/customers-small.json')
            .toPromise()
            .then(res => <User[]>res.data)
            .then(data => { return data; });
    }

    getCustomersMedium() {
        return this.http.get<any>('assets/showcase/data/customers-medium.json')
            .toPromise()
            .then(res => <User[]>res.data)
            .then(data => { return data; });
    }

    getCustomersLarge() {
        
        return this.http.get(`${GlobalVariable.BASE_API_URL}/userList`,
    
        { responseType: 'text'}
          ).toPromise()
               .then(data => { return JSON.parse(data); });
    }

    getCustomersXLarge() {
        return this.http.get<any>('assets/showcase/data/customers-xlarge.json')
            .toPromise()
            .then(res => <User[]>res.data)
            .then(data => { return data; });
    }

}