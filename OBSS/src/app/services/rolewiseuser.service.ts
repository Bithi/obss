import { Subject } from 'rxjs';
import { GlobalVariable } from '../global/global';
import { RoleWiseUser } from '../models/rolewiseuser';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { DataService } from '../data/data.service';
import { Utility } from '../util/utility';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoleWiseUserService {

  public addToTheList = new Subject<any>();

  apiBaseUrl = GlobalVariable.BASE_API_URL;

    public roleWiseUser:RoleWiseUser = new RoleWiseUser();

    key: string = "toast";
    severity: string;
    detailMsg: string;
  
  constructor(
      private http: HttpClient,
      public router: Router,
      private dataService: DataService,
      private utility: Utility) {}


      public insertRoleWiseUser(jsonMessage) {
        return this.http.post(GlobalVariable.BASE_API_URL + '/roleWiseUser',
            btoa(jsonMessage), {
                responseType: 'text'
            }
        ).subscribe(
            (res) => {
  
                const msg = JSON.parse(atob(res));
  
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong.";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    sessionStorage.removeItem('isLoggedin');
                    // // this.showProgressSpin = false;
                    return;
                }
                // this.showProgressSpin = false;
                //sessionStorage.setItem('isLoggedin', 'true');
                //sessionStorage.setItem('loggedinUser', JSON.stringify(msg.payLoad[0]));
                // this.router.navigate(['/dashboard']);
                this.severity = 'success';
                this.detailMsg = "Insertion Successful";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                
  
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                // this.showProgressSpin = false;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }

    getUserList(){
      // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
      //     catchError(this.handleError)
      // );
      return this.http.get(`${this.apiBaseUrl}/userList`,
      
   { responseType: 'text'}
      )
  };

  getMyRoleList(){
    // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
    //     catchError(this.handleError)
    // );
    return this.http.get(`${this.apiBaseUrl}/roleList`,
    
 { responseType: 'text'}
    )
};


passTheValue(roleWIseUser){
  var data = JSON.parse(JSON.stringify(roleWIseUser));
  this.addToTheList.next(data);
}

public updateRoleWiseUser(jsonMessage){
  debugger;
  this.http.post(GlobalVariable.BASE_API_URL+'/roleWiseUser',
  btoa(jsonMessage)
, { responseType: 'text'}
  )
  .subscribe(
    (res ) => {
            const msg = JSON.parse( atob(res) );
            console.log(res);
            
            if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                 this.severity = 'error';
                 this.detailMsg = "Error occured";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 sessionStorage.removeItem('isLoggedin');
                // this.showProgressSpin = false;
                return;
            }
            // this.showProgressSpin = false;
            this.severity = 'success';
            this.detailMsg = "Updated Successfully";
            this.utility.ShowToast(this.key,this.severity,this.detailMsg);
            
            
        
    },
    err => {
         this.severity = 'error';
         this.detailMsg = "Server Error: " + err.message;
        // // this.showProgressSpin = false;
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);
        return;
    }
);
}

public deleteRoleWiseUser(jsonMessage){
  return this.http.post(GlobalVariable.BASE_API_URL + '/roleWiseUser',
  btoa(jsonMessage), {
      responseType: 'text'
  }
  )
  .subscribe(
  (res ) => {
          const msg = JSON.parse( atob(res) );
          console.log(res);
          
          if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
               this.severity = 'error';
               this.detailMsg = "Error occured";
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
               sessionStorage.removeItem('isLoggedin');
              //this.showProgressSpin = false;
              return;
          }
          // this.showProgressSpin = false;
         this.severity = 'success';
         this.detailMsg = "Deleted Successfully";
         this.utility.ShowToast(this.key,this.severity,this.detailMsg);    
  },
  err => {
       this.severity = 'error';
       this.detailMsg = "Server Error: " + err.message;
       // this.showProgressSpin = false;
       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
      return;
  }
  );
  }

  public showRoleWiseUserInfo(jsonMessage){
    return this.http.post(GlobalVariable.BASE_API_URL+'/roleWiseUser',
       btoa(jsonMessage)
   , { responseType: 'text'}
       )
  }

}