import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Utility } from '../util/utility';
import { DataService } from '../data/data.service';
import { GlobalVariable } from '../global/global';
import { User } from '../models/user';
import { Subject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DropdownInfoService {

    apiBaseUrl = GlobalVariable.BASE_API_URL;

  constructor(
    private http:HttpClient,
    public router: Router,
    private dataService: DataService,
    private utility:Utility) { }

    getVendors(){
        // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
        //     catchError(this.handleError)
        // );
        return this.http.get(`${this.apiBaseUrl}/vendors`,
        
     { responseType: 'text'}
        )
    };


    getMenuList(){
      // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
      //     catchError(this.handleError)
      // );
      return this.http.get(`${this.apiBaseUrl}/menus`,
      
   { responseType: 'text'}
      )
  };

  getModule(){
    // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
    //     catchError(this.handleError)
    // );
    return this.http.get(`${this.apiBaseUrl}/module`,
    
 { responseType: 'text'}
    )
};
   

    matchUserId(userId: string){
      // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
      //     catchError(this.handleError)
      // );
      if(userId){
      return this.http.get(`${this.apiBaseUrl}/matchUserId/${userId}`,
      
      )
      }
  };

  getVendorServiceType(){
    return this.http.get(`${this.apiBaseUrl}/vendorServiceType`,
    
  { responseType: 'text'}
    )
  }


    getContracts(vendorId: string) {
        // return this.http.get(`${this.apiBaseUrl}/v1/contract/${vendorId}`).pipe(
        //   catchError(this.handleError)
        // );
        if(vendorId){
          return this.http.get(`${this.apiBaseUrl}/contract/${vendorId}`,
        
          { responseType: 'text'}
             )
        
        }
      
      };

      getContractBraches(contractId: string) {
        // return this.http.get(`${this.apiBaseUrl}/v1/branch/${contractId}`).pipe(
        //   catchError(this.handleError)
        // );
        if(contractId){
        return this.http.get(`${this.apiBaseUrl}/branch/${contractId}`,
        
        { responseType: 'text'}
           )
        }
      };


    private handleError(error: HttpErrorResponse){
        if(error.error instanceof ErrorEvent){
            console.error('An error occured:',error.error.message);
        }else{
            console.error(`Backend returned code ${error.status},`+`body was: ${error.error}`);
        }
        return throwError('Something bad happened. Please try again later.');
    };
    getContractInfos(){
      // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
      //     catchError(this.handleError)
      // );
      return this.http.get(`${this.apiBaseUrl}/contractInfoList`,
      
   { responseType: 'text'}
      )
  };

  getBranchNameList(){
    // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
    //     catchError(this.handleError)
    // );
    return this.http.get(`${this.apiBaseUrl}/getBrNameList`,
    
 { responseType: 'text'}
    )
};

    
  
    
}
