import {  Injectable } from '@angular/core';
  import {HttpClient} from '@angular/common/http';
  import { Router} from '@angular/router';
  import { Utility} from '../util/utility';
  import { DataService} from '../data/data.service';
  import {  GlobalVariable} from '../global/global';
  import { Subject,  Observable} from 'rxjs';
  import { Category } from '../models/category';
  
  
  @Injectable({
    providedIn: 'root'
  })
  export class ProfileService {
  
   
    key: string = "toast";
    severity: string;
    detailMsg: string;
    
    constructor(private http: HttpClient,public router: Router,private dataService: DataService,private utility: Utility) {}

    saveBook(formData){

        return this.http.post(GlobalVariable.BASE_API_URL + '/bookWithImage',
           formData, {
                responseType: 'text'
            }).subscribe(
            (res) => {
                const msg = JSON.parse(atob(res));
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    return;
                }
                this.severity = 'success';
                this.detailMsg = "Book inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg); 
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }

    updateUser(formData){

        return this.http.post(GlobalVariable.BASE_API_URL + '/userWithImage',
           formData, {
                responseType: 'text'
            }).subscribe(
            (res) => {
                const msg = JSON.parse(atob(res));
                if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                    this.severity = 'error';
                    this.detailMsg = "There is something wrong";
                    this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                    return;
                }

                this.severity = 'success';
                this.detailMsg = "Book inserted Successfully";
                this.utility.ShowToast(this.key,this.severity,this.detailMsg); 
            },
            err => {
                this.severity = 'error';
                this.detailMsg = "Server Error: " + err.message;
                this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                return;
            }
        );
    }

    getCategoryList(){
        return this.http.get(GlobalVariable.BASE_API_URL+'/categoryList',{ responseType: 'text'});
    }
    getWriterList(){
        return this.http.get(GlobalVariable.BASE_API_URL+'/writerList',{ responseType: 'text'});
    }
    // public getAllBooks(jsonMessage){
    //     return this.http.post(GlobalVariable.BASE_API_URL+'/book',
    //        btoa(jsonMessage)
    //    , { responseType: 'text'}
    //        )
    //  }
     getAllBooks(){
        // return this.http.get(`${this.apiBaseUrl}/v1/vendors`).pipe(
        //     catchError(this.handleError)
        // );
        return this.http.get(`${GlobalVariable.BASE_API_URL}/bookList`,
        
     { responseType: 'text'}
        )
    };
    public requestBook(jsonMessage){
        debugger;
        this.http.post(GlobalVariable.BASE_API_URL+'/trBook',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Book requested Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }
      public returnBook(jsonMessage){
        this.http.post(GlobalVariable.BASE_API_URL+'/trBook',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Book returned Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }

      public claimBook(jsonMessage){
        this.http.post(GlobalVariable.BASE_API_URL+'/trBook',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Book claimed Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }

      public approveBook(jsonMessage){
        this.http.post(GlobalVariable.BASE_API_URL+'/trBook',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Book approved Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }
      
      public rejectBook(jsonMessage){
        debugger;
        this.http.post(GlobalVariable.BASE_API_URL+'/trBook',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "Book rejected Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }
    getBooksList(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showBooksByUser/'+userId,{ responseType: 'text'});
    }
    
    toBeReturnBookList(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/toBeReturnBookList/'+userId,{ responseType: 'text'});
    }
    getToClaimBooks(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/getToClaimBooks/'+userId,{ responseType: 'text'});
    }
    
    requestedBookList(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/requestedBookList/'+userId,{ responseType: 'text'});
    }
    getBooksDetails(bookId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showBooksDetailById/'+bookId,{ responseType: 'text'});
    }
    getUserDetails(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showDetailByUser/'+userId,{ responseType: 'text'});
    }
    getReviewist(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showReviewForUser/'+userId,{ responseType: 'text'});
    }
    getAllBooksDetails(userId){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showAllBooksDetail/'+userId,{ responseType: 'text'});
    } 
    showReqBookStatus(requster,book){
        return this.http.get(GlobalVariable.BASE_API_URL+'/showReqBookStatus/'+requster+'/'+book,{ responseType: 'text'});
    } 
    
    deleteBook(jsonMessage){
        return this.http.post(GlobalVariable.BASE_API_URL + '/book',
        btoa(jsonMessage), {
             responseType: 'text'
         }).subscribe(
         (res) => {
             const msg = JSON.parse(atob(res));
             if (!msg.payLoad || msg.payLoad.length < 1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error") {
                 this.severity = 'error';
                 this.detailMsg = "There is something wrong";
                 this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                 return;
             }
             this.severity = 'success';
             this.detailMsg = "Book inserted Successfully";
             this.utility.ShowToast(this.key,this.severity,this.detailMsg); 
         },
         err => {
             this.severity = 'error';
             this.detailMsg = "Server Error: " + err.message;
             this.utility.ShowToast(this.key,this.severity,this.detailMsg);
             return;
         }
     );
    }

    public giveReview(jsonMessage){
        debugger;
        this.http.post(GlobalVariable.BASE_API_URL+'/review',
        btoa(jsonMessage)
    , { responseType: 'text'}
        )
        .subscribe(
          (res ) => {
                  const msg = JSON.parse( atob(res) );
                  console.log(res);
                  
                  if(!msg.payLoad || msg.payLoad.length<1 || !msg.payLoad[0]|| msg.dataHeader.reponseStatus=="error"){
                       this.severity = 'error';
                       this.detailMsg = "Error occured";
                       this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                       sessionStorage.removeItem('isLoggedin');
                      // this.showProgressSpin = false;
                      return;
                  }
                  // this.showProgressSpin = false;
                  this.severity = 'success';
                  this.detailMsg = "User Reviewed Successfully";
                  this.utility.ShowToast(this.key,this.severity,this.detailMsg);
                  
                  
              
          },
          err => {
               this.severity = 'error';
               this.detailMsg = "Server Error: " + err.message;
              // // this.showProgressSpin = false;
               this.utility.ShowToast(this.key,this.severity,this.detailMsg);
              return;
          }
     );
      }

  
  }