import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot) { 
        
         
        var roleWiseMenu = sessionStorage.getItem('roleWiseMenu');
        if(roleWiseMenu!=null){
            if (sessionStorage.getItem('isLoggedin')==="true") {

                if(this._isContains(JSON.parse(roleWiseMenu), route.data.path)){
                    return true;
                }
                
              
            }
        }
       
       
        this.router.navigate(['']);
        return false;
    
}
    _isContains(json, value) {
       for (var i=0;i<json.length;i++){
           if(json[i].url===value){
               return true;
           }
       }
       return false;

       
     }
}