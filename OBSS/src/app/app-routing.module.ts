import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboradComponent } from './pages/dashboard/dashborad/dashborad.component';
import { LoginComponent } from './pages/login/login.component';
import { MenuInfoInsertComponent } from './pages/menuInfo/menu-info-insert/menu-info-insert.component';
import { RoleInfoInsertComponent } from './pages/roleInfo/role-info-insert/role-info-insert.component';
import { MenuAssignedToRoleInsertComponent } from './pages/MenuAssignedToRole/menu-assigned-to-role-insert/menu-assigned-to-role-insert.component';

import { ProfileComponent } from './pages/profile/profile.component';

import { RoleWiseUserInsertComponent } from './pages/role-wise-user/role-wise-user-insert/role-wise-user-insert.component';
import { ApprovalComponent } from './pages/approval/approval.component';
import { CategoryInsertComponent } from './pages/category/category-create/category-create.component';
import { WriterInsertComponent } from './pages/writer/writer-create/writer-create.component';
import { AuthGuard } from './util/auth.guard';
import { UserInfoInsertComponent } from './pages/user-info/user-info-insert/user-info-insert.component';

const routes: Routes = [
  { path: 'OBSS', component: DashboradComponent, canActivate: [AuthGuard], data: { path: 'dashboard' }  },
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboradComponent, canActivate: [AuthGuard], data: { path: 'dashboard' }  },
  { path: 'userinfo', component: UserInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'userinfo' }  },
  { path: 'menuinfo', component: MenuInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'menuinfo' }  },
  { path: 'roleinfo' , component: RoleInfoInsertComponent, canActivate: [AuthGuard], data: { path: 'roleinfo' } },
  { path: 'menuassignedtorole' , component: MenuAssignedToRoleInsertComponent, canActivate: [AuthGuard], data: { path: 'menuassignedtorole' } },
  { path: 'userProfile' , component: ProfileComponent, canActivate: [AuthGuard], data: { path: 'userProfile' , trap: '1'}  },
  { path: 'rolewiseuser' , component:RoleWiseUserInsertComponent, canActivate: [AuthGuard], data: { path: 'rolewiseuser' } },
  { path: 'userApproval' , component:ApprovalComponent, canActivate: [AuthGuard], data: { path: 'userApproval' } },
  { path: 'category' , component:CategoryInsertComponent, canActivate: [AuthGuard], data: { path: 'category' } },
  { path: 'writer' , component:WriterInsertComponent, canActivate: [AuthGuard], data: { path: 'writer' } }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
