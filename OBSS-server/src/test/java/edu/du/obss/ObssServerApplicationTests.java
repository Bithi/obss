package edu.du.obss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:spring-beans.xml")
@ComponentScan("edu.du.*")
public class ObssServerApplicationTests extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	
	    return application.sources(ObssServerApplicationTests.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ObssServerApplicationTests.class, args);
	}

}