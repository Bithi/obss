package edu.du.obss.util;


import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.Review;

@Service
public class DataProcessor<T> {
	
	private Gson gson;
	{
		 gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
	}

	public DataHeader buildMessageHeader(DataHeader dataHeader,String status,String reponseCode,String description) {
		
		dataHeader.setReponseCode(reponseCode);
		dataHeader.setReponseStatus(status);
		dataHeader.setReponseTimestamp(new Date());
		dataHeader.setDescription(description==null?Constant.STR_EMPTY.toString():description);
		return dataHeader;
		
	}
	public List<?> getObjectListFromPayload(List<?> payLoad,Type type ){
		return gson.fromJson(gson.toJson(payLoad), type);
	}
	@SuppressWarnings("hiding")
	public <T> Object getObjectFromJsonString(String jsonString,Type type ){
		return gson.fromJson(jsonString, type);
	}
	public String getResponseAsJson(Data message) {
		return gson.toJson(message);
	}
	public String getObjectFromJsonString(List<Review> revList) {
		// TODO Auto-generated method stub
		return gson.toJson(revList);
	}
}

