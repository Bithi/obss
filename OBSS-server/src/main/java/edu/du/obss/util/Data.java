package edu.du.obss.util;

import java.util.List;

public class Data {

	private DataHeader dataHeader;
	private List<?> payLoad;
	public DataHeader getDataHeader() {
		return dataHeader;
	}
	public void setDataHeader(DataHeader dataHeader) {
		this.dataHeader = dataHeader;
	}
	public List<?> getPayLoad() {
		return payLoad;
	}
	public void setPayLoad(List<?> payLoad) {
		this.payLoad = payLoad;
	}
	
	
}
