package edu.du.obss.util;

import java.util.Date;

public class DataHeader {
	
	public Date getRequestTimestamp() {
		return requestTimestamp;
	}
	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getReponseTimestamp() {
		return reponseTimestamp;
	}
	public void setReponseTimestamp(Date reponseTimestamp) {
		this.reponseTimestamp = reponseTimestamp;
	}
	public String getReponseStatus() {
		return reponseStatus;
	}
	public void setReponseStatus(String reponseStatus) {
		this.reponseStatus = reponseStatus;
	}
	public String getReponseCode() {
		return reponseCode;
	}
	public void setReponseCode(String reponseCode) {
		this.reponseCode = reponseCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	
	private Date requestTimestamp;
	private String userId;
	private String password;
	private String actionType;
	private Date reponseTimestamp;
	private String reponseStatus;
	private String reponseCode;
	private String description; 	//If error occurs details of error will be sent here


	
	
	

}
