package edu.du.obss.constants;

public enum Constant {

	STR_EMPTY(""),
	FW_SLASH("/"),
	BW_SLASH("\\"),
	SP_ROLE("SBWALLET.Pkg_Wall_Data_Manipulate.P_RoleInfo"),
	SP_USER_ROLE("SBWALLET.Pkg_Wall_Data_Manipulate.P_UserRole"),
	SP_INSERT_USER("OBSS.p_insert_USER"),
	SP_INSERT_ROLE("OBSS.p_insert_ROLE"),
	SP_INSERT_MENU("OBSS.p_insert_MENU"),
	SP_INSERT_MENUASSIGNEDTOROLE("OBSS.p_insert_MENUASSIGNEDTOROLE"),
	SP_INSERT_ROLEASSIGNEDTOUSER("OBSS.p_insert_ROLEASSIGNEDTOUSER"),
	SP_SHOW_MENU("OBSS.P_showMenu"),
	SP_USERLIST("OBSS.P_userList"),
	SP_INSERT_WRITER("OBSS.p_WRITER"),
	SP_INSERT_CATEGORY("OBSS.p_CATEGORY"),
	SP_INSERT_BOOK("OBSS.p_BOOK"),
	SP_INSERT_WRITER_BOOK("OBSS.p_BOOKWRITER");
	
	private final String value;

    Constant(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

	
}
