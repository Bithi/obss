package edu.du.obss.constants;

public enum ActionTypes {
	
	SELECT("SELECT"),
	SELECT_ALL("SELECT_ALL"),
	SAVE("SAVE"),
	SELECT_ALL_DATA("S"),
	INSERT("I"),
	UPDATE("UPDATE"),
	USER_LOGIN("USER_LOGIN"),
	SHOW_MENU("SHOW_MENU"),
	USER_REGISTER("USER_REGISTER"),
	SELECT_APPROVAL_REQUEST_LIST("SELECT_APPROVAL_REQUEST_LIST"),
	DELETE("DELETE"),
	APPROVE_USERS("APPROVE_USERS"),
	REJECT_USERS("REJECT_USERS"),
	REQUEST("REQUEST"),
	REJECT("REJECT"),
	APPROVE("APPROVE"),
	RETURN("RETURN"),
	CLAIM("CLAIM");
	
	
	private final String value;

	ActionTypes(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
