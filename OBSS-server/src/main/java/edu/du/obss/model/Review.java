package edu.du.obss.model;
import java.io.Serializable;  
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.mock.web.MockMultipartFile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "USERREVIEW")
public class Review implements Serializable  {
	  
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "USERREVIEWTAB")
	@SequenceGenerator(name="USERREVIEWTAB", initialValue =1,allocationSize=1,sequenceName = "REVIEW_SEQ")
//	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "U_ID")
	private String user;

	@Column(name = "REVIEW")
	private String review;
	
	@Column(name = "RATING")
	private Integer rating;

	@Column(name = "REVIEWER")
	private Integer reviewer;
	
	@Column(name = "REVIEWDATE")
	private Timestamp reviewDate;
	
	@Column(name = "USER_NAME",insertable = false, updatable = false)
    private String  reviewerName;
	
	@Column(name = "IMG_URL",insertable = false, updatable = false)
    private String  img_url;
	
	@Transient
    private MockMultipartFile  image;
	

	
	
	

}
