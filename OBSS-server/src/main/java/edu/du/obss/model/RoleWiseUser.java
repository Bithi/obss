package edu.du.obss.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
@Getter
@Setter
@Entity
@Table(name = "ROLEASSIGNEDTOUSER")
public class RoleWiseUser
{
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "USER_ID")
    private Integer u_id;
    @Column(name = "ROLE_ID")
    private Integer r_id;
    
   
}