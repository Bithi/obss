package edu.du.obss.model;

import java.sql.Timestamp;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.mock.web.MockMultipartFile;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "TRBOOK")

public class TrBook {
	// USER_ID
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TRBOOKTAB")
	@SequenceGenerator(name="TRBOOKTAB", initialValue =1,allocationSize=1,sequenceName = "TRBOOK_SEQ")
//	@Column(name = "ID", nullable = false)
	private Integer Id;

	@Column(name = "BOOK", nullable = false)
	private Integer book;

	@Column(name = "REQUESTER")
	private Integer requester;

	@Column(name = "OWNER")
	private Integer owner;

	@Column(name = "TRDATE")
	private Timestamp trDate;
	
	@Column(name = "STATUS")
	private String status;
	
	@Transient
	private Integer uId;
	
	@Transient
	private String user_name;
	
	@Transient
	private String nid;
	
	@Transient
	private String address;
	
	@Transient
	private String phone;
	
	
	@Transient
	private String book_name;
	
	@Transient
	private String bimg;
	
	@Transient
	private String uimg;
	
	@Transient
    private MockMultipartFile  bookimg;

	@Transient
    private MockMultipartFile  userimg;
	
	public static HashMap<String,String> rsMap = new HashMap<String,String>();
	
	
	public static HashMap<String, String> getRsMap() {
		if(rsMap.isEmpty()) {
			rsMap.put("Id", "ID");
			rsMap.put("book", "BOOK");
			rsMap.put("requester", "REQUESTER");
			rsMap.put("owner", "OWNER");
			rsMap.put("trDate", "TRDATE");
			rsMap.put("status", "STATUS");
			
			rsMap.put("bimg", "BIMG");
			rsMap.put("book_name", "BOOK_NAME");
			rsMap.put("user_name", "USER_NAME");
			rsMap.put("uId", "U_ID");
			rsMap.put("uimg", "UIMG");
			rsMap.put("nid", "NID");
			rsMap.put("address", "ADDRESS");
			rsMap.put("phone", "PHONE");
		}
		return rsMap;
	}
}
