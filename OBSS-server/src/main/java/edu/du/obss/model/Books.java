package edu.du.obss.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.mock.web.MockMultipartFile;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "BOOKS")
public class Books {
	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "BOOK_NAME")
	private String bookName;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "PUBLISHDATE")
	private Timestamp publishDate;
	
	@Column(name = "BOOK_CATEGORY")
	private Integer bookCategory;

	@Column(name = "BOOK_VERSION")
	private String bookVersion;
	
	@Column(name = "ISSN")
	private String issn;
	
	@Column(name = "IMG_URL")
	private String imgUrl;

	@Column(name = "OWNER")
	private Integer owner;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "CREATEDATE")
	private Timestamp createDate;

	@Column(name = "CREATEDBY")
	private Integer createdBy;
	
	@Column(name = "UPDATEDATE")
	private Timestamp updateDate;

	@Column(name = "UPDATEDBY")
	private Integer updatedBy;
	
	@Transient
    private MockMultipartFile  image;
	
	@Transient
    private String  ownerName;
	
	@Transient
    private String  writer;
	
	@Transient
    private String  category;
	
	public static HashMap<String,String> rsMap = new HashMap<String,String>();
	
	
	public static HashMap<String, String> getRsMap() {
		if(rsMap.isEmpty()) {
			rsMap.put("id", "ID");
			rsMap.put("bookName", "BOOK_NAME");
			rsMap.put("description", "DESCRIPTION");
			rsMap.put("publishDate", "PUBLISHDATE");
			rsMap.put("bookCategory", "BOOK_CATEGORY");
			rsMap.put("bookVersion", "BOOK_VERSION");
			rsMap.put("issn", "ISSN");
			rsMap.put("imgUrl", "IMG_URL");
			rsMap.put("owner", "OWNER");
			rsMap.put("status", "STATUS");
			rsMap.put("createDate", "CREATEDATE");
			rsMap.put("createdBy", "CREATEDBY");
			rsMap.put("updateDate", "UPDATEDATE");
			rsMap.put("updatedBy", "UPDATEDBY");
			rsMap.put("ownerName", "owner_name");
			rsMap.put("writer", "writer");
			rsMap.put("category", "category");
		}
		return rsMap;
	}

	
	

}
