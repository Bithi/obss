package edu.du.obss.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MENU")
public class Menu {

	@Id
	@Column(name = "m_id", nullable = false)
	private Integer m_id;
	
	@Column(name = "MENUNAME")
	private String menuName;
	
	@Column(name = "PARENTMENUID")
	private Integer parentMenuId;
	
	@Column(name = "URL")
	private String url;
	
	@Column(name = "ICON")
	private String icon;
	
	@Column(name = "COMPONENT")
	private String component;
	
	@Column(name = "MODULE")
	private String module;
	
	public Menu() {
		
	}

	public Menu(Integer m_id, String menuName, Integer parentMenuId, String url, String icon, String component,
			String module) {
		super();
		this.m_id = m_id;
		this.menuName = menuName;
		this.parentMenuId = parentMenuId;
		this.url = url;
		this.icon = icon;
		this.component = component;
		this.module = module;
	}


	public Integer getM_id() {
		return m_id;
	}

	public void setM_id(Integer m_id) {
		this.m_id = m_id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Integer getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	
	
}
