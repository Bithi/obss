package edu.du.obss.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROLE")
public class Role {

	@Id
	@Column(name = "R_ID", nullable = false)
	private Integer r_id;
	
	@Column(name = "NAME")
	private String roleName;
	
	@Column(name = "STATUS")
	private String status;

	public Role(String roleName, String status) {
		this.roleName = roleName;
		this.status = status;
	}

	public Role() {
		
	}

	public Role(Integer id, String roleName2, String status2) {
		// TODO Auto-generated constructor stub
		r_id = id;
		roleName = roleName2;
		status = status2;
	}

	public Integer getR_id() {
		return r_id;
	}

	public void setR_id(Integer r_id) {
		this.r_id = r_id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
