package edu.du.obss.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WRITER")

public class Writer {
	@Id
	@Column(name = "ID", nullable = false)
	private Integer Id;

	@Column(name = "NAME")
	private String writerName;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	
	@Column(name = "STATUS")
	private String status;

	public Writer() {
		super();
	}

	
	
	public Writer(Integer id, String writerName, String description,  String status) {
		super();
		this.Id = id;
		this.writerName = writerName;
		this.description = description;
		this.status = status;
	}



	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		this.Id = id;
	}
	
	
	
}
