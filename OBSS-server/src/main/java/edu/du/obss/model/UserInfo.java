package edu.du.obss.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.mock.web.MockMultipartFile;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@DynamicUpdate
@Entity
@Table(name = "USER_INFO")
public class UserInfo {
	// USER_ID
	@Id
	@Column(name = "ID", nullable = false)
	private Integer Id;

	@Column(name = "USER_ID", nullable = false)
	private String userId;

	@Column(name = "USER_NAME")
	private String userName;

	// PASSWORD
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "PHONE")
	private String phone;

	// USER_STATUS
	@Column(name = "USER_STATUS")
	private String userStatus;

	// FAIL_COUNT
	@Column(name = "FAIL_COUNT")
	private Integer failCount;

	// PWD_CHG_DT
	@Column(name = "PWD_CHG_DT")
	private Timestamp passwordChangeDate;

	@Column(name = "NID")
	private String nid;

	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name = "GENDER")
	private String gender;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "CREATEDATE")
	private Timestamp createDate;

	@Column(name = "CREATEDBY")
	private Integer createdBy;
	
	@Column(name = "UPDATEDATE")
	private Timestamp updateDate;

	@Column(name = "UPDATEDBY")
	private Integer updatedBy;

	@Column(name = "APPROVEDBY")
	private Integer approvedBy;
	
	@Column(name = "IMG_URL")
	private String img_url;
	
	@Column(name = "EDUCATION")
	private String education;
	
	@Column(name = "DESIGNATION")
	private String designation;
	
	@Column(name = "ORGANIZATION")
	private String organization;
	
	@Column(name = "NOTES")
	private String notes;

	@Transient
    private MockMultipartFile  image;
	
//	public UserInfo() {
//		super();
//	}
//
//	public UserInfo( String userId, String userName, String password, String phone, String userStatus,
//			Integer failCount, Timestamp passwordChangeDate, String nid, String address, String gender, String email,
//			Timestamp createDate, Integer createdBy, Timestamp updateDate, Integer updatedBy, String img_url,
//			String education, String designation, String organization,  String notes) {
//		super();
//		this.userId = userId;
//		this.userName = userName;
//		this.password = password;
//		this.phone = phone;
//		this.userStatus = userStatus;
//		this.failCount = failCount;
//		this.passwordChangeDate = passwordChangeDate;
//		this.nid = nid;
//		this.address = address;
//		this.gender = gender;
//		this.email = email;
//		this.createDate = createDate;
//		this.createdBy = createdBy;
//		this.updateDate = updateDate;
//		this.updatedBy = updatedBy;
//		this.img_url = img_url;
//		this.education = education;
//		this.designation = designation;
//		this.organization = organization;
//		this.notes = notes;
//	}
//
//	public Integer getId() {
//		return Id;
//	}
//
//	public void setId(Integer id) {
//		Id = id;
//	}
//
//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getUserStatus() {
//		return userStatus;
//	}
//
//	public void setUserStatus(String userStatus) {
//		this.userStatus = userStatus;
//	}
//
//	public Integer getFailCount() {
//		return failCount;
//	}
//
//	public void setFailCount(Integer failCount) {
//		this.failCount = failCount;
//	}
//
//	public Timestamp getPasswordChangeDate() {
//		return passwordChangeDate;
//	}
//
//	public void setPasswordChangeDate(Timestamp passwordChangeDate) {
//		this.passwordChangeDate = passwordChangeDate;
//	}
//
//	public String getNid() {
//		return nid;
//	}
//
//	public void setNid(String nid) {
//		this.nid = nid;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public void setAddress(String address) {
//		this.address = address;
//	}
//
//	public String getGender() {
//		return gender;
//	}
//
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public Timestamp getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Timestamp createDate) {
//		this.createDate = createDate;
//	}
//
//	public Integer getCreatedBy() {
//		return createdBy;
//	}
//
//	public void setCreatedBy(Integer createdBy) {
//		this.createdBy = createdBy;
//	}
//
//	public Timestamp getUpdateDate() {
//		return updateDate;
//	}
//
//	public void setUpdateDate(Timestamp updateDate) {
//		this.updateDate = updateDate;
//	}
//
//	public Integer getUpdatedBy() {
//		return updatedBy;
//	}
//
//	public void setUpdatedBy(Integer updatedBy) {
//		this.updatedBy = updatedBy;
//	}
//
//	public Integer getApprovedBy() {
//		return approvedBy;
//	}
//
//	public void setApprovedBy(Integer approvedBy) {
//		this.approvedBy = approvedBy;
//	}
//
//	public String getImg_url() {
//		return img_url;
//	}
//
//	public void setImg_url(String img_url) {
//		this.img_url = img_url;
//	}
//
//	public String getEducation() {
//		return education;
//	}
//
//	public void setEducation(String education) {
//		this.education = education;
//	}
//
//	public String getDesignation() {
//		return designation;
//	}
//
//	public void setDesignation(String designation) {
//		this.designation = designation;
//	}
//
//	public String getOrganization() {
//		return organization;
//	}
//
//	public void setOrganization(String institution) {
//		this.organization = organization;
//	}
//
//	public String getNotes() {
//		return notes;
//	}
//
//	public void setNotes(String notes) {
//		this.notes = notes;
//	}

	

}
