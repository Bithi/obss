package edu.du.obss.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKWRITER")
public class WriterBook {
	@Id
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "WRITER")
	private Integer writer;
	
	@Column(name = "BOOK")
	private Integer book;

	public WriterBook(Integer id, Integer writer, Integer book) {
		super();
		id = id;
		this.writer = writer;
		this.book = book;
	}

	public WriterBook() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		id = id;
	}

	public Integer getWriter() {
		return writer;
	}

	public void setWriter(Integer writer) {
		this.writer = writer;
	}

	public Integer getBook() {
		return book;
	}

	public void setBook(Integer book) {
		this.book = book;
	}
	
	
}
