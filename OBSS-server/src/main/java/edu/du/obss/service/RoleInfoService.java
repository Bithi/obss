package edu.du.obss.service;


import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.Role;
import edu.du.obss.util.CommonService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RoleInfoService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	
	
	public Role saveOrUpdateRoleInfo(Role roleInfo) throws Exception {
		final Session session = sessionFactory.openSession();
		
		System.err.print("---"+roleInfo.getRoleName()+" "+roleInfo.getStatus()+"---");
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_ROLE.toString(), Role.class);
			call.registerParameter("pR_ID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pNAME", String.class, ParameterMode.IN).bindValue((roleInfo.getRoleName()==null)?"":roleInfo.getRoleName());
			call.registerParameter("pSTATUS", String.class, ParameterMode.IN).bindValue((roleInfo.getStatus()==null)?"":roleInfo.getStatus());			
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return roleInfo;
	}
	
	public List<Role> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Role> roleId = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			roleId = session.createQuery("from Role", Role.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return roleId;
    }
	
	
	public List<Role> getRoleList() {
		return commonService.getDataList(new Role(),null);
	}
	
	
	public Role updateRoleInfo(Role roleInfo) throws Exception {

		Integer Id = 0;
		if(roleInfo.getR_id()!=null) {
			Id = roleInfo.getR_id();
		}
	
		String roleName = roleInfo.getRoleName();
		String status = roleInfo.getStatus();
		
		Role roleOne = new Role(Id,roleName,status);
		return updateRole(roleOne);
	}
	
	public Role updateRole(Role roleOne) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(roleOne);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return roleOne;
	}
	
	
	public Role DeleteRoleInfo(Role role) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			session.delete(role);
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return role;
	}

}
