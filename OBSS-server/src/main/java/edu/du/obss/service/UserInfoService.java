package edu.du.obss.service;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.procedure.ProcedureCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.Books;
import edu.du.obss.model.Review;
import edu.du.obss.model.RoleWiseUser;
import edu.du.obss.model.UserInfo;
import edu.du.obss.util.CommonService;

@Service
public class UserInfoService
{
    private static final Logger log;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CommonService commonService;
    @Autowired
    private RoleWiseUserService roleWiseUserService;
    @Autowired
    private MailSendService mailSendService;
    @Value("${file.store.location}")
	private String fileStoreLocation;
    static {
        log = LoggerFactory.getLogger((Class)UserInfoService.class);
    }
    
    public List<UserInfo> getUserInfoList() {
        return (List<UserInfo>)this.commonService.getDataList((Object)new UserInfo(), (Map)null);
    }
    
    public List<UserInfo> userList(final int approved) throws Exception {
        final Transaction transaction = null;
        Session session = null;
        try {
            session = this.sessionFactory.openSession();
            final ProcedureCall call = session.createStoredProcedureCall(Constant.SP_USERLIST.toString(), new Class[] { UserInfo.class });
            final String action = "USER_LIST";
            call.registerParameter("pAPPROVED", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)approved);
            call.registerParameter("pIN_ACTION", (Class)String.class, ParameterMode.IN).bindValue((Object)action);
            call.registerParameter("pUSERID", (Class)String.class, ParameterMode.IN).bindValue((Object)"");
            call.registerParameter("pPASSWORD", (Class)String.class, ParameterMode.IN).bindValue((Object)"");
            call.registerParameter("OUT_RESULT", (Class)Object.class, ParameterMode.REF_CURSOR);
            call.execute();
            List<UserInfo> userList;
            try {
                userList = (List<UserInfo>)call.getResultList();
            }
            catch (NoResultException nre) {
                userList = null;
            }
            if (session != null) {
                session.close();
            }
            return userList;
        }
        catch (Exception e) {
           UserInfoService.log.info(e.getLocalizedMessage());
            if (session != null && session.isOpen()) {
                session.close();
            }
            throw e;
        }
        finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public UserInfo login(final String userId, final String password) throws Exception {
        final Transaction transaction = null;
        Session session = null;
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            final byte[] digest = md.digest();
            final String encodedPassword = new String(Hex.encode(digest));
            session = this.sessionFactory.openSession();
            final ProcedureCall call = session.createStoredProcedureCall(Constant.SP_USERLIST.toString(), new Class[] { UserInfo.class });
            final String action = "LOGIN";
            final int approved = 1;
            call.registerParameter("pAPPROVED", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)approved);
            call.registerParameter("pIN_ACTION", (Class)String.class, ParameterMode.IN).bindValue((Object)action);
            call.registerParameter("pUSERID", (Class)String.class, ParameterMode.IN).bindValue((Object)userId);
            call.registerParameter("pPASSWORD", (Class)String.class, ParameterMode.IN).bindValue((Object)encodedPassword);
            call.registerParameter("OUT_RESULT", (Class)Object.class, ParameterMode.REF_CURSOR);
            call.execute();
            List<UserInfo> userList;
            try {
                userList = (List<UserInfo>)call.getResultList();
                
            }
            catch (NoResultException nre) {
                userList = null;
            }
            if (session != null) {
                session.close();
            }
//            for (int j = 0; j < userList.size(); j++) {
//            	 if(userList.get(j).getImg_url()!=null)
//	           {
//	            	File image = new File (fileStoreLocation+ "/User/"+userList.get(j).getImg_url());
//	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
//	            	userList.get(j).setImage(multipartFile);;
//	            }
//	            
//	        }
            return userList.get(0);
        }
        catch (Exception e) {
//            UserInfoService.log.info(e.getLocalizedMessage());
            if (session != null && session.isOpen()) {
                session.close();
            }
            throw e;
        }
        finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
    public UserInfo saveUserInfo(final UserInfo userInfo) throws Exception {
    	userInfo.setCreateDate(userInfo.getCreateDate()==null?new Timestamp(System.currentTimeMillis()):new Timestamp(userInfo.getCreateDate().getTime()));
    	userInfo.setCreatedBy(0) ;
        userInfo.setUpdateDate(userInfo.getUpdateDate()==null?new Timestamp(0):new Timestamp(userInfo.getUpdateDate().getTime())) ;
    	userInfo.setUpdatedBy(0);
    	userInfo.setFailCount(0);;
        final String education = userInfo.getEducation();
        final String designation = userInfo.getDesignation();
        final String organization = userInfo.getOrganization();
        final String notes = userInfo.getNotes();
        final Date date = new Date();
        final long time = date.getTime();
        userInfo.setPasswordChangeDate( new Timestamp(time));
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(userInfo.getPassword().getBytes());
            final byte[] bytes = md.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; ++i) {
                sb.append(Integer.toString((bytes[i] & 0xFF) + 256, 16).substring(1));
            }
            userInfo.setPassword(sb.toString());
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return this.saveOrUpdateUserInfoX(userInfo);
    }
    
    public UserInfo updateAllUserInfo(final UserInfo userInfo) throws Exception {
    	userInfo.setCreateDate(userInfo.getCreateDate()==null?new Timestamp(System.currentTimeMillis()):new Timestamp(userInfo.getCreateDate().getTime()));
    	userInfo.setCreatedBy(0) ;
        userInfo.setUpdateDate(userInfo.getUpdateDate()==null?new Timestamp(System.currentTimeMillis()):new Timestamp(userInfo.getUpdateDate().getTime())) ;
    	userInfo.setUpdatedBy(0);
    	userInfo.setFailCount(0);;
        final String education = userInfo.getEducation();
        final String designation = userInfo.getDesignation();
        final String organization = userInfo.getOrganization();
        final String notes = userInfo.getNotes();
        final Date date = new Date();
        final long time = date.getTime();
        userInfo.setPasswordChangeDate( new Timestamp(time));
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(userInfo.getPassword().getBytes());
            final byte[] bytes = md.digest();
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; ++i) {
                sb.append(Integer.toString((bytes[i] & 0xFF) + 256, 16).substring(1));
            }
            userInfo.setPassword(sb.toString());
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return this.saveOrUpdateUserInfoX(userInfo);
    }
    
    public UserInfo updateUser(UserInfo userInfo) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(userInfo);
			session.flush();

		} 
		catch (Exception e) {
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return userInfo;
	}
    
    public UserInfo saveOrUpdateUserInfoX(final UserInfo userInfo) throws Exception {
        final Session session = this.sessionFactory.openSession();
        String status = null;
        try {
            UserInfoService.log.debug("Calling SP");
            session.getTransaction().begin();
            final ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_USER.toString(), new Class[] { UserInfo.class });
            call.registerParameter("pUSER_ID", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getUserId() == null) ? "" : userInfo.getUserId()));
            call.registerParameter("pUSER_NAME", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getUserName() == null) ? "" : userInfo.getUserName()));
            call.registerParameter("pPASSWORD", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getPassword() == null) ? "" : userInfo.getPassword()));
            call.registerParameter("pPHONE", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getPhone() == null) ? "" : userInfo.getPhone()));
            call.registerParameter("pUSER_STATUS", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getUserStatus() == null) ? "" : userInfo.getUserStatus()));
            call.registerParameter("pFAIL_COUNT", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)userInfo.getFailCount());
            call.registerParameter("pPWD_CHG_DT", (Class)Timestamp.class, ParameterMode.IN).bindValue((Object)userInfo.getPasswordChangeDate());
            call.registerParameter("pNID", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getNid() == null) ? "" : userInfo.getNid()));
            call.registerParameter("pADDRESS", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getAddress() == null) ? "" : userInfo.getAddress()));
            call.registerParameter("pGENDER", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getGender() == null) ? "" : userInfo.getGender()));
            call.registerParameter("pCREATEDATE", (Class)Timestamp.class, ParameterMode.IN).bindValue((Object)userInfo.getCreateDate());
            call.registerParameter("pUPDATEDATE", (Class)Timestamp.class, ParameterMode.IN).bindValue((Object)userInfo.getUpdateDate());
            call.registerParameter("pCREATEBY", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)userInfo.getCreatedBy());
            call.registerParameter("pUPDATEBY", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)userInfo.getUpdatedBy());
            call.registerParameter("pEMAIL", (Class)String.class, ParameterMode.IN).bindValue((Object)((userInfo.getEmail() == null) ? "" : userInfo.getEmail()));
            call.registerParameter("InStatus", (Class)String.class, ParameterMode.OUT);
            call.execute();
            final String opStatus = (String)call.getOutputParameterValue("InStatus");
            UserInfoService.log.info(opStatus);
            status = opStatus;
            if (opStatus.indexOf("Error") > -1) {
                throw new Exception(opStatus);
            }
            else {
            	
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            if (session != null && session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            return null;
        }
        finally {
            if (session != null && session.isOpen()) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null && session.isOpen()) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return userInfo;
    }
    
    public UserInfo saveOrUpdateUserInfo(final UserInfo userInfo) throws Exception {
        final Session session = this.sessionFactory.openSession();
        System.err.print("--------- " + userInfo.getId() + "----");
        try {
            session.beginTransaction();
            session.saveOrUpdate((Object)userInfo);
//            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return userInfo;
    }
    
    public UserInfo deleteAllUserInfo(final UserInfo userInfo) throws Exception {
        final Session session = this.sessionFactory.openSession();
        System.err.print("--------- " + userInfo.getId() + "----");
        try {
            session.beginTransaction();
            session.delete((Object)userInfo);
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return userInfo;
    }
    
    public List<UserInfo> approve(final List<UserInfo> users) {
        final List<UserInfo> approvedList = new ArrayList<UserInfo>();
        final RoleWiseUser roleWiseUser = new RoleWiseUser();
        final List<RoleWiseUser> roleWiseUserList = new ArrayList<RoleWiseUser>();
        for (int i = 0; i < users.size(); ++i) {
            try {
                approvedList.add(this.saveOrUpdateUserInfo(users.get(i)));
                roleWiseUser.setU_id(users.get(i).getId());
                roleWiseUser.setR_id(Integer.valueOf(2));
                roleWiseUserList.add(this.roleWiseUserService.saveOrUpdateRoleWiseUser(roleWiseUser));
                mailSendService.sendMailForApproval(users.get(i));
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return approvedList;
    }
    
    public List<UserInfo> reject(final List<UserInfo> users) {
        final List<UserInfo> rejectedList = new ArrayList<UserInfo>();
        for (int i = 0; i < users.size(); ++i) {
            try {
            	rejectedList.add(this.saveOrUpdateUserInfo(users.get(i)));
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return rejectedList;
    }
    
    
    public List<UserInfo> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<UserInfo> users = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			users = session.createQuery("from UserInfo where userStatus='A' ", UserInfo.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return users;
    }
    
    public UserInfo showDetailByUser(int user) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    UserInfo userInfo =  new UserInfo();
	    session.getTransaction().commit();
	    
	    try {
	    	userInfo = session.createQuery("from UserInfo where Id ="+user, UserInfo.class)
		            .getSingleResult();
	    	if(userInfo.getImg_url()!=null) {
	            	File image = new File (fileStoreLocation+ "/User/"+userInfo.getImg_url());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	userInfo.setImage(multipartFile);;
	            }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return userInfo;
	}
    
    @SuppressWarnings("unchecked")
	public List<UserInfo> showAllAdmins() throws Exception {
		final Session session = sessionFactory.openSession();
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		// List<Review> reviews = new ArrayList<Review>();
		UserInfo  userInfo = new UserInfo();
		try {
			
			
			userInfoList = (List<UserInfo>) session.createNativeQuery(
					"select u.* from user_info u inner join ROLEASSIGNEDTOUSER r on(u.id=r.user_id) where R.ROLE_ID='1'"
							,UserInfo.class)
					 .list();

	        
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
//		String json = new Gson().toJson(reviews);
		return userInfoList;
	}
}