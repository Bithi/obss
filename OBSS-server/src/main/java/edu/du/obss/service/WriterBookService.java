package edu.du.obss.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.Books;
import edu.du.obss.model.Writer;
import edu.du.obss.model.WriterBook;
import edu.du.obss.util.CommonService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WriterBookService {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private CommonService commonService;
	
	public WriterBook saveOrUpdateWriter(WriterBook writerBook) throws Exception {
		final Session session = sessionFactory.openSession();
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_WRITER_BOOK.toString(), WriterBook.class);
			call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pBOOK", int.class, ParameterMode.IN).bindValue((writerBook.getBook()==null)?0:writerBook.getBook());
			call.registerParameter("pWRITER", int.class, ParameterMode.IN).bindValue((writerBook.getWriter()==null)?0:writerBook.getWriter());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return writerBook;
	}
	
	public WriterBook updateWriterBook(WriterBook writerBook) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(writerBook);
			session.flush();

		} 
		catch (Exception e) {
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return writerBook;
	}
	public WriterBook DeleteWriterBook(WriterBook writerBook) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
//			book = (Books)session.load(Books.class,book.getId());
//			    session.delete(book);
			session.delete(writerBook);
			
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return writerBook;
	}
	
	public List<WriterBook> showWriterBookByBook(WriterBook writerBook) {
		List<WriterBook> list = new ArrayList<WriterBook>();
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			// list = (List<ContractInfo>) session.load(ContractInfo.class, venId);
			Criteria crit = session.createCriteria(WriterBook.class);
			crit.add(Restrictions.eq("book", writerBook.getBook()));
			list = crit.list();
			
			
			
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return list;
	}
}
