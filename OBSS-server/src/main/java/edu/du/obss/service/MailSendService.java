package edu.du.obss.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.result.ResultSetOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import edu.du.obss.model.Books;
import edu.du.obss.model.UserInfo;
import edu.du.obss.util.MailSender;
import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class MailSendService {
	@Autowired
	private MailSender mailsender;
	 @Autowired
	    private SessionFactory sessionFactory;
		@Autowired
		private UserInfoService userInfoService;

	public void sendMailForApproval(UserInfo toUser) throws Exception {
		log.info("start mail sending ForApproval");
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			if(toUser.getEmail()!=null && toUser.getEmail()!="") {
			 mailsender.sendMail(toUser.getEmail(),"Approval of Registration", "Dear "+toUser.getUserName()+", Registration is approved by OBSS.");
				
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending ForApproval");
	}
	
	public void sendMailForRegistration(UserInfo toUser) throws Exception {
		log.info("start mail sending For Registration");
		List<UserInfo> adminList = new ArrayList<UserInfo>();
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			adminList = userInfoService.showAllAdmins();
			for (UserInfo admin : adminList) {
			if(admin.getEmail()!=null && admin.getEmail()!="") {
			 mailsender.sendMail(admin.getEmail(),"Request of Registration","Dear Admin, Some Registration is waiting for approval.");
			}
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending For Registration");
	}
	public void sendMailForRequestOfBooks(UserInfo owner, UserInfo requester, Books book ) throws Exception {
		log.info("start mail sending ForApproval");
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			if(owner.getEmail()!=null && owner.getEmail()!="") {
			 mailsender.sendMail(owner.getEmail(),"Request of Books", "Dear " +owner.getUserName()+","
			 		+requester.getUserName()+" has requested for Book "+book.getBookName());
			 
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending ForApproval");
	}
	
	public void sendMailForReturnOfBooks(UserInfo owner, UserInfo requester, Books book ) throws Exception {
		log.info("start mail sending ForApproval");
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			if(owner.getEmail()!=null && owner.getEmail()!="") {
			 mailsender.sendMail(owner.getEmail(),"Return of Books", "Dear " +owner.getUserName()+","
			 		+requester.getUserName()+" has returned your Book "+book.getBookName()+""
			 				+ ". If you didn't get your book, please mail to bithi.cse.cu@gmail.com");
			 
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending ForApproval");
	}
	
	public void sendMailForApprovalOfBooks(UserInfo owner, UserInfo requester, Books book ) throws Exception {
		log.info("start mail sending ForApproval");
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			if(requester.getEmail()!=null && requester.getEmail()!="") {
			 mailsender.sendMail(requester.getEmail(),"Approval of Books", "Dear " +requester.getUserName()+","
			 		+ " Your request for Book "+book.getBookName()+" is approved by Book Owner "+owner.getUserName()+".");
			 
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending ForApproval");
	}
	
	public void sendMailForClaimed(UserInfo owner, UserInfo requester, Books book ) throws Exception {
		log.info("start mail sending For Claimed");
		List<UserInfo> adminList = new ArrayList<UserInfo>();
		//get list of object for sending mail
		Session session = null;
		try{
			session = sessionFactory.openSession();
			adminList = userInfoService.showAllAdmins();
			for (UserInfo admin : adminList) {
				
				if(admin.getEmail()!=null && admin.getEmail()!="") {
					 mailsender.sendMail(admin.getEmail(),"Request of Claim", "Dear Admin, "+requester.getUserId()+" is being claimed "
					 		+ " by "+owner.getUserId()+" for the Book "+book.getBookName()+". Please, take necessary action.");
						
					}
				
			}
			
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
			session.close();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
		} finally {
			if (session != null) {
				session.close();
			}

		}
		log.info("finish mail sending ForApproval");
	}
	

}
