package edu.du.obss.service;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ParameterMode;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;

import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.Books;
import edu.du.obss.model.Menu;
import edu.du.obss.model.Review;
import edu.du.obss.model.TrBook;
import edu.du.obss.model.UserInfo;
import edu.du.obss.model.Writer;
import edu.du.obss.util.CommonService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BookService {
	@Autowired
	private SessionFactory sessionFactory;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	@Autowired
	private CommonService commonService;
	@Autowired
	private DataProcessor<?> messageProcessor;
	 @Autowired
	    private MailSendService mailSendService;
	
	public List<Books> getBookList() {
		return commonService.getDataList(new Books(),null);
	}
	
	public List<Books> getLabels() throws Exception{
		final Session session = sessionFactory.openSession();
        session.beginTransaction();

        // We read labels record from database using a simple Hibernate
        // query, the Hibernate Query Language (HQL).
        List<Books> labels = null;
        session.getTransaction().commit();
        
        try {
			session.beginTransaction();
			labels = session.createQuery("from Books", Books.class)
		            .list();
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

        return labels;
    }
	
	public Books saveOrUpdateBook(Books book) throws Exception {
		final Session session = sessionFactory.openSession();
		
		String status = null;
		String id = null;
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			java.util.Date reportDate;
			reportDate = new Date();
			if(book.getId()!=null) {
				//update
				id = String.valueOf(book.getId());
				book.setUpdateDate(new Timestamp(reportDate.getTime()));
				session.update(book);
				session.flush();
			}else {
				ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_BOOK.toString(), Books.class);
				
				book.setPublishDate(new Timestamp(reportDate.getTime()));
				book.setCreateDate(new Timestamp(reportDate.getTime()));
				book.setUpdateDate(new Timestamp(reportDate.getTime()));
				
				call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
				call.registerParameter("pBOOK_NAME", String.class, ParameterMode.IN).bindValue(((book.getBookName()==null)?"":book.getBookName()));
				call.registerParameter("pDESCRIPTION", String.class, ParameterMode.IN).bindValue(((book.getDescription()==null)?"":book.getDescription()));
				call.registerParameter("pPUBLISHDATE", Timestamp.class, ParameterMode.IN).bindValue(book.getPublishDate());
				call.registerParameter("pBOOK_CATEGORY", int.class, ParameterMode.IN).bindValue((book.getBookCategory()==null)?0:book.getBookCategory());
				call.registerParameter("pBOOK_VERSION", String.class, ParameterMode.IN).bindValue((book.getBookVersion()==null)?"":book.getBookVersion());
				call.registerParameter("pISSN", String.class, ParameterMode.IN).bindValue((book.getIssn()==null)?"":book.getIssn());
				call.registerParameter("pIMG_URL", String.class, ParameterMode.IN).bindValue((book.getImgUrl()==null)?"":book.getImgUrl());
				call.registerParameter("pOWNER", int.class, ParameterMode.IN).bindValue((book.getOwner()==null)?0:book.getOwner());
				call.registerParameter("pSTATUS", String.class, ParameterMode.IN).bindValue((book.getStatus()==null)?"":book.getStatus());
				call.registerParameter("pCREATEDATE", Timestamp.class, ParameterMode.IN).bindValue(book.getCreateDate());
				call.registerParameter("pCREATEDBY", int.class, ParameterMode.IN).bindValue((book.getCreatedBy()==null)?0:book.getCreatedBy());
				call.registerParameter("pUPDATEDATE", Timestamp.class, ParameterMode.IN).bindValue((book.getUpdateDate()));
				call.registerParameter("pUPDATEDBY", int.class, ParameterMode.IN).bindValue((book.getUpdatedBy()==null)?0:book.getUpdatedBy());
				
				
				
				
				call.registerParameter("InStatus", String.class, ParameterMode.OUT);
				call.execute();
				String opStatus = (String) call.getOutputParameterValue("InStatus");
				String[] parts = opStatus.split("-");
				id = parts[0]; 
				status = parts[1]; 
				log.info(status);
				if (opStatus.indexOf("Error") > -1) {
					throw new Exception(opStatus);
					
				}
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		book.setId(Integer.parseInt(id));
		return book;
	}
	
	
	public Books updateBook(Books bookOne) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(bookOne);
			session.flush();

		} 
		catch (Exception e) {
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return bookOne;
	}
	
	public Books DeleteBookInfo(Books book) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
//			book = (Books)session.load(Books.class,book.getId());
//			    session.delete(book);
			session.delete(book);
			File file=new File(fileStoreLocation+"/User/"+book.getOwner()+"/library/"+book.getImgUrl());
			if(file.exists()){
				if(file.delete()){
					System.out.println("File deleted successfully");
				}else{
					System.out.println("Fail to delete file");
				}
			}
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return book;
	}
	
	
//	
	
	public List<Books> showBooksByUser(String user) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<Books> list = null;
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    long usr = Long.parseLong(user);
	    try {
//			session.beginTransaction();
	    	list = (List<Books>) session
			        .createNativeQuery("select * from books where owner = "+usr+" and status='A'",Books.class)
			        .list();
			for (int j = 0; j < list.size(); j++) {
	            if(list.get(j).getImgUrl()!=null)
	            {
	            	File image = new File (fileStoreLocation+ "/User/" + user + "/library/"+list.get(j).getImgUrl());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	list.get(j).setImage(multipartFile); 
	            }
	            
	        }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<TrBook> showToBeReturnByOwner(int req) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<TrBook> trBooklist = new ArrayList<TrBook>();
	    String sql = "";
	    TrBook trBook = new TrBook();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    try {
//			session.beginTransaction();
	    	sql = "select TR.*, B.IMG_URL bImg, B.BOOK_NAME, U.USER_NAME,U.ID u_id, U.IMG_URL uImg, U.NID from TRBOOK tr\r\n" + 
			        		" join Books b on(tr.book=b.id)\r\n" + 
			        		" join User_info u on(u.ID=tr.owner) where tr.requester="+req+" and tr.status='approved'";
	    	
	    	trBooklist=(List<TrBook>)commonService.getDataListSQLWithMapping(trBook.getClass(), sql,trBook.getRsMap());
	    	
			for (int j = 0; j < trBooklist.size(); j++) {
				if(trBooklist.get(j).getBimg()!= null && !trBooklist.get(j).getBimg().equals("")) {
					
				
	            	File image = new File (fileStoreLocation+"/User/"+trBooklist.get(j).getOwner()+"/library/"+trBooklist.get(j).getBimg());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	trBooklist.get(j).setBookimg(multipartFile);
	            
				}
			if(trBooklist.get(j).getUimg()!= null && !trBooklist.get(j).getUimg().equals("")) {
				
			
            	File uImage = new File (fileStoreLocation+"/User/"+trBooklist.get(j).getUimg());
            	MockMultipartFile multipartFileForUser = new MockMultipartFile(uImage.getName(),new FileInputStream(uImage));
            	trBooklist.get(j).setUserimg(multipartFileForUser);
            }
	    
	            
	        }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return trBooklist;
	}
	
	@SuppressWarnings("unchecked")
	public List<TrBook> showToClaimBooks(int owner) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<TrBook> trBooklist = new ArrayList<TrBook>();
	    String sql = "";
	    TrBook trBook = new TrBook();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    try {
//			session.beginTransaction();
	    	sql = "select TR.*, B.IMG_URL bImg, B.BOOK_NAME, U.USER_NAME,U.ID u_id, U.IMG_URL uImg, U.NID from TRBOOK tr\r\n" + 
			        		" join Books b on(tr.book=b.id)\r\n" + 
			        		" join User_info u on(u.ID=tr.owner) where tr.owner="+owner+" and tr.status='approved'";
	    	
	    	trBooklist=(List<TrBook>)commonService.getDataListSQLWithMapping(trBook.getClass(), sql,trBook.getRsMap());
	    	
			for (int j = 0; j < trBooklist.size(); j++) {
				if(trBooklist.get(j).getBimg()!= null && !trBooklist.get(j).getBimg().equals("")) {
					
				
	            	File image = new File (fileStoreLocation+"/User/"+owner+"/library/"+trBooklist.get(j).getBimg());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	trBooklist.get(j).setBookimg(multipartFile);
	            
				}
			if(trBooklist.get(j).getUimg()!= null && !trBooklist.get(j).getUimg().equals("")) {
				
			
            	File uImage = new File (fileStoreLocation+"/User/"+trBooklist.get(j).getUimg());
            	MockMultipartFile multipartFileForUser = new MockMultipartFile(uImage.getName(),new FileInputStream(uImage));
            	trBooklist.get(j).setUserimg(multipartFileForUser);
            }
	    
	            
	        }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return trBooklist;
	}
	@SuppressWarnings("unchecked")
	public List<TrBook> showTrBooksByOwner(int owner) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    List<TrBook> trBooklist = new ArrayList<TrBook>();
	    String sql = "";
	    TrBook trBook = new TrBook();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    try {
//			session.beginTransaction();
	    	sql = "select TR.*, B.IMG_URL bImg, B.BOOK_NAME, U.USER_NAME,U.ID u_id, U.IMG_URL uImg, U.NID,U.ADDRESS, U.PHONE  from TRBOOK tr\r\n" + 
			        		" join Books b on(tr.book=b.id)\r\n" + 
			        		" join User_info u on(u.ID=tr.requester) where tr.owner="+owner+" and tr.status='request'";
	    	
	    	trBooklist=(List<TrBook>)commonService.getDataListSQLWithMapping(trBook.getClass(), sql,trBook.getRsMap());
	    	
			for (int j = 0; j < trBooklist.size(); j++) {
				if(trBooklist.get(j).getBimg()!= null && !trBooklist.get(j).getBimg().equals("")) {
					
				
	            	File image = new File (fileStoreLocation+"/User/"+owner+"/library/"+trBooklist.get(j).getBimg());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	trBooklist.get(j).setBookimg(multipartFile);
	            
				}
			if(trBooklist.get(j).getUimg()!= null && !trBooklist.get(j).getUimg().equals("")) {
				
			
            	File uImage = new File (fileStoreLocation+"/User/"+trBooklist.get(j).getUimg());
            	MockMultipartFile multipartFileForUser = new MockMultipartFile(uImage.getName(),new FileInputStream(uImage));
            	trBooklist.get(j).setUserimg(multipartFileForUser);
            }
	    
	            
	        }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return trBooklist;
	}
	
	
	public Object showBooksDetailById(int id) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Object object = new Object();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			object = session.createNativeQuery("select b.*, C.NAME category,W.id writer from books b left join\r\n" + 
					"category c on(b.BOOK_CATEGORY = c.ID)\r\n" + 
					"left join BOOKWRITER bw on(bw.BOOK = b.id)\r\n" + 
					"left join writer w on(w.id=bw.WRITER) where b.id="+id)
		            .list();
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return object;
	}
	
	
	
	public List<Books> showAllBooksDetails(int userId) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    Data message = null;
	    String SQL = "";
	    Books book = new Books();
		List<Books> bookList = new ArrayList<Books>();
	    try {
	    	SQL= "select b.*, C.NAME category,W.name writer, U.USER_NAME owner_name from books b left join category c on(b.BOOK_CATEGORY = c.ID) \r\n" + 
					"left join BOOKWRITER bw on(bw.BOOK = b.id)\r\n" + 
					"left join writer w on(w.id=bw.WRITER) \r\n" + 
					"left join user_info u on(b.OWNER=u.id) where u.id!="+userId+" and b.status='A' ";
		
//			
			bookList=(List<Books>)commonService.getDataListSQLWithMapping(book.getClass(), SQL,book.getRsMap());
			for (int j = 0; j < bookList.size(); j++) {
				
				if(bookList.get(j).getImgUrl()!= null && !bookList.get(j).getImgUrl().equals("")) {
					
				}
	            	File image = new File (fileStoreLocation+"/User/"+bookList.get(j).getOwner()+"/library/"+bookList.get(j).getImgUrl());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	bookList.get(j).setImage(multipartFile);
	            }
	            
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.close();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}

	}

	    return bookList;
	}
	
	public String showReqBookStatus(int requester, int book) throws Exception{
		final Session session = sessionFactory.openSession();
	    session.beginTransaction();
	    String status = "";
	    List<TrBook> trBookList = new ArrayList<TrBook>();
	    TrBook trBook = new TrBook();
	    // We read labels record from database using a simple Hibernate
	    // query, the Hibernate Query Language (HQL).
	    session.getTransaction().commit();
	    
	    try {
			session.beginTransaction();
			trBookList = (List<TrBook>) session.createNativeQuery("SELECT * FROM (select * from TRBOOK where requester ="+requester+" and book ="+book+"  order by trdate desc) where rownum = 1 ",TrBook.class)
		            .getResultList();
			if(trBookList.size()>0) {

				status = trBookList.get(0).getStatus();
			}
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if(session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
			session.close();
			
			}
		}
	    	return status;
	}
	
	public TrBook trBook(TrBook trBook) throws  Exception {
		final Session session = sessionFactory.openSession();
		Books book = new Books();
		UserInfo requester = new UserInfo();
		UserInfo user = new UserInfo();
		try {
			trBook.setTrDate(new Timestamp(System.currentTimeMillis()));
			session.beginTransaction();
				session.saveOrUpdate(trBook);
				if(trBook.getStatus().equals("request")) {
					book =  (Books) session.get(Books.class, trBook.getBook());
					requester =  (UserInfo) session.get(UserInfo.class, trBook.getRequester());
					user =  (UserInfo) session.get(UserInfo.class, trBook.getOwner());
					mailSendService.sendMailForRequestOfBooks(user,requester,book);
				}
				
				
				if(trBook.getStatus().equals("approved")) {
					book =  (Books) session.get(Books.class, trBook.getBook());
					book.setStatus("I");
					this.updateBook(book);
					requester =  (UserInfo) session.get(UserInfo.class, trBook.getRequester());
					user =  (UserInfo) session.get(UserInfo.class, trBook.getOwner());
					mailSendService.sendMailForApprovalOfBooks(user,requester,book);
				}
				else if(trBook.getStatus().equals("returned")) {
					book =  (Books) session.get(Books.class, trBook.getBook());
					book.setStatus("A");
					this.updateBook(book);
					requester =  (UserInfo) session.get(UserInfo.class, trBook.getRequester());
					user =  (UserInfo) session.get(UserInfo.class, trBook.getOwner());
					mailSendService.sendMailForReturnOfBooks(user,requester,book);
				}
				else if(trBook.getStatus().equals("claimed")) {
					book =  (Books) session.get(Books.class, trBook.getBook());
					requester =  (UserInfo) session.get(UserInfo.class, trBook.getRequester());
					user =  (UserInfo) session.get(UserInfo.class, trBook.getOwner());
					mailSendService.sendMailForClaimed(user,requester,book);
				}
			
			session.flush();

		} 
		catch (Exception e) {
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return trBook;
	}
	
	
	
}
