package edu.du.obss.service;

import java.util.Map;
import java.util.List;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.Session;
import javax.persistence.ParameterMode;
import edu.du.obss.constants.Constant;
import edu.du.obss.model.Category;
import org.slf4j.LoggerFactory;
import edu.du.obss.util.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class CategoryService
{
    private static final Logger log;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CommonService commonService;
    
    static {
        log = LoggerFactory.getLogger((Class)CategoryService.class);
    }
    
    public Category saveOrUpdateCategory(final Category category) throws Exception {
        final Session session = this.sessionFactory.openSession();
        String status = null;
        try {
            CategoryService.log.debug("Calling SP");
            session.getTransaction().begin();
            final ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_CATEGORY.toString(), new Class[] { Category.class });
            call.registerParameter("pID", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)0);
            call.registerParameter("pNAME", (Class)String.class, ParameterMode.IN).bindValue((Object)((category.getName() == null) ? "" : category.getName()));
            call.registerParameter("pDESCRIPTION", (Class)String.class, ParameterMode.IN).bindValue((Object)((category.getDescription() == null) ? "" : category.getDescription()));
            call.registerParameter("InStatus", (Class)String.class, ParameterMode.OUT);
            call.execute();
            final String opStatus = (String)call.getOutputParameterValue("InStatus");
            CategoryService.log.info(opStatus);
            status = opStatus;
            if (opStatus.indexOf("Error") > -1) {
                throw new Exception(opStatus);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            if (session != null && session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            return null;
        }
        finally {
            if (session != null && session.isOpen()) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null && session.isOpen()) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return category;
    }
    
    public Category updateCategory(final Category CategoryOne) throws Exception {
        final Session session = this.sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate((Object)CategoryOne);
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause());
            session.getTransaction().rollback();
            if (e.getCause().toString().contains("ConstraintViolationException")) {
                throw new Exception("Constraint Violation occured");
            }
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return CategoryOne;
    }
    
    public Category DeleteCategoryInfo(final Category category) throws Exception {
        final Session session = this.sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete((Object)category);
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return category;
    }
    
    public List<Category> getCategoryList() {
        return (List<Category>)this.commonService.getDataList((Object)new Category(), (Map)null);
    }
    
    public List<Category> getLabels() throws Exception {
        final Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        List<Category> labels = null;
        session.getTransaction().commit();
        try {
            session.beginTransaction();
            labels = (List<Category>)session.createQuery("from Category", (Class)Category.class).list();
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return labels;
    }
}