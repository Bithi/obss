package edu.du.obss.service;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import edu.du.obss.model.Books;
import edu.du.obss.model.Menu;
import edu.du.obss.model.Review;
import edu.du.obss.model.TrBook;
import edu.du.obss.util.CommonService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReviewService {
	@Autowired
	private SessionFactory sessionFactory;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	@Autowired
	private CommonService commonService;

	public List<Review> getReviewList() {
		return commonService.getDataList(new Review(), null);
	}

	@SuppressWarnings("unchecked")
	public List<Review> showReviewByUser(int id) throws Exception {
		final Session session = sessionFactory.openSession();
		List<Review> reviews = new ArrayList<Review>();
		// We read labels record from database using a simple Hibernate
		// query, the Hibernate Query Language (HQL).
		// List<Review> reviews = new ArrayList<Review>();
		Review rev = new Review();
		try {
			
			
			 reviews = (List<Review>) session.createNativeQuery(
					"select ur.*, UI.USER_NAME ,UI.IMG_URL from USERREVIEW ur join user_info ui on(ur.reviewer=ui.id) where ur.u_id="
							+ id,Review.class)
					 .list();

			for (int j = 0; j < reviews.size(); j++) {
	            if(reviews.get(j).getImg_url()!= null && !reviews.get(j).getImg_url().equals("")) {
	            	File image = new File (fileStoreLocation + "/User/" + reviews.get(j).getImg_url());
	            	MockMultipartFile multipartFile = new MockMultipartFile(image.getName(),new FileInputStream(image));
	            	reviews.get(j).setImage(multipartFile);
	            }
	            
	        }
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
//		String json = new Gson().toJson(reviews);
		return reviews;
	}
	public Review saveOrUpdateReview(Review review) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			review.setReviewDate(new Timestamp(System.currentTimeMillis()));
			session.beginTransaction();
			if(review.getId()== null) {
				session.save(review);
			}
			else {
				session.saveOrUpdate(review);
			}
			session.flush();

		} 
		catch (Exception e) {
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return review;
	}
	
//	public Review saveOrUpdateReview(Review review) throws Exception {
//		final Session session = sessionFactory.openSession();
//
//		try {
//			session.beginTransaction();
//			session.saveOrUpdate(review);
//			session.flush();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//
//			System.out.println(e.getCause());
//			session.getTransaction().rollback();
//			if (e.getCause().toString().contains("ConstraintViolationException")) {
//				throw new Exception("Constraint Violation occured");
//			} else
//				throw e;
//		} finally {
//			if (session != null) {
//				if (session.getTransaction().isActive()) {
//					session.getTransaction().commit();
//				}
//				session.close();
//
//			}
//
//		}
//		return review;
//	}

	public Review DeleteReview(Review review) throws Exception {
		final Session session = sessionFactory.openSession();
		// System.err.print("--------- "+poroInfo.getPoroCode()+"----");

		try {
			session.beginTransaction();
			// book = (Books)session.load(Books.class,book.getId());
			// session.delete(book);
			session.delete(review);

			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return review;
	}

}
