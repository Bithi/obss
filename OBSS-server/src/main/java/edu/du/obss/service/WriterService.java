package edu.du.obss.service;

import java.util.Map;
import java.util.List;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.Session;
import javax.persistence.ParameterMode;
import edu.du.obss.constants.Constant;
import edu.du.obss.model.Writer;
import org.slf4j.LoggerFactory;
import edu.du.obss.util.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class WriterService
{
    private static final Logger log;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CommonService commonService;
    
    static {
        log = LoggerFactory.getLogger((Class)WriterService.class);
    }
    
    public Writer saveOrUpdateWriter(final Writer writer) throws Exception {
        final Session session = this.sessionFactory.openSession();
        String status = null;
        try {
            WriterService.log.debug("Calling SP");
            session.getTransaction().begin();
            final ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_WRITER.toString(), new Class[] { Writer.class });
            call.registerParameter("pID", (Class)Integer.TYPE, ParameterMode.IN).bindValue((Object)0);
            call.registerParameter("pNAME", (Class)String.class, ParameterMode.IN).bindValue((Object)((writer.getWriterName() == null) ? "" : writer.getWriterName()));
            call.registerParameter("pDESCRIPTION", (Class)String.class, ParameterMode.IN).bindValue((Object)((writer.getDescription() == null) ? "" : writer.getDescription()));
            call.registerParameter("pSTATUS", (Class)String.class, ParameterMode.IN).bindValue((Object)((writer.getStatus() == null) ? "" : writer.getStatus()));
            call.registerParameter("InStatus", (Class)String.class, ParameterMode.OUT);
            call.execute();
            final String opStatus = (String)call.getOutputParameterValue("InStatus");
            WriterService.log.info(opStatus);
            status = opStatus;
            if (opStatus.indexOf("Error") > -1) {
                throw new Exception(opStatus);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            if (session != null && session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            return null;
        }
        finally {
            if (session != null && session.isOpen()) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null && session.isOpen()) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return writer;
    }
    
    public Writer updateWriterInfo(final Writer writerInfo) throws Exception {
        Integer Id = 0;
        if (writerInfo.getId() != null) {
            Id = writerInfo.getId();
        }
        final String name = writerInfo.getWriterName();
        final String description = writerInfo.getDescription();
        final String status = writerInfo.getStatus();
        final Writer writerOne = new Writer(Id, name, description, status);
        return this.updateWriter(writerOne);
    }
    
    public Writer updateWriter(final Writer writerOne) throws Exception {
        final Session session = this.sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate((Object)writerOne);
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause());
            session.getTransaction().rollback();
            if (e.getCause().toString().contains("ConstraintViolationException")) {
                throw new Exception("Constraint Violation occured");
            }
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return writerOne;
    }
    
    public Writer DeleteWriterInfo(final Writer writer) throws Exception {
        final Session session = this.sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete((Object)writer);
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return writer;
    }
    
    public List<Writer> getWriterList() {
        return (List<Writer>)this.commonService.getDataList((Object)new Writer(), (Map)null);
    }
    
    public List<Writer> getLabels() throws Exception {
        final Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        List<Writer> labels = null;
        session.getTransaction().commit();
        try {
            session.beginTransaction();
            labels = (List<Writer>)session.createQuery("from Writer", (Class)Writer.class).list();
            session.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            if (session != null) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().commit();
                }
                session.close();
            }
        }
        if (session != null) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().commit();
            }
            session.close();
        }
        return labels;
    }
}