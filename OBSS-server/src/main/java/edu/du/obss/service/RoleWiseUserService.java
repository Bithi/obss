package edu.du.obss.service;


import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.du.obss.constants.Constant;
import edu.du.obss.model.RoleWiseUser;
import edu.du.obss.util.CommonService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RoleWiseUserService {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private CommonService commonService;
	
	
	public RoleWiseUser saveOrUpdateRoleWiseUser(RoleWiseUser roleWiseUser) throws Exception {
		final Session session = sessionFactory.openSession();
		
		
		String status = null;
		
		try {
			log.debug("Calling SP");
			session.getTransaction().begin();
			ProcedureCall call = session.createStoredProcedureCall(Constant.SP_INSERT_ROLEASSIGNEDTOUSER.toString(), RoleWiseUser.class);
			call.registerParameter("pID", int.class, ParameterMode.IN).bindValue(0);
			call.registerParameter("pU_ID", int.class, ParameterMode.IN).bindValue((roleWiseUser.getU_id()==null)?0:roleWiseUser.getU_id());
			call.registerParameter("pR_ID", int.class, ParameterMode.IN).bindValue((roleWiseUser.getR_id()==null)?0:roleWiseUser.getR_id());	
//			call.registerParameter("pAPPROVEDBY", int.class, ParameterMode.IN).bindValue((roleWiseUser.getApprovedBy()==null)?0:roleWiseUser.getApprovedBy());
			call.registerParameter("InStatus", String.class, ParameterMode.OUT);
			call.execute();
			String opStatus = (String) call.getOutputParameterValue("InStatus");
			log.info(opStatus);
			status = opStatus;
			if (opStatus.indexOf("Error") > -1) {
				throw new Exception(opStatus);
				
			}

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			if (session != null && session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}

				session.close();
			}

		}
		return roleWiseUser;
	}
	
	
	
	
	
	public RoleWiseUser updateRoleWiseUserInfo(RoleWiseUser mATR) throws  Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.saveOrUpdate(mATR);
			session.flush();

		} 
		catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			
			System.out.println(e.getCause());
			session.getTransaction().rollback();
			if(e.getCause().toString().contains("ConstraintViolationException")) {
				throw new Exception("Constraint Violation occured");
			}
			else
				throw e;
		}
		finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return mATR;
	}

	
	public RoleWiseUser DeleteRoleWiseUser(RoleWiseUser roleWiseUser) throws Exception {
		final Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();
			session.delete(roleWiseUser);
			session.flush();

		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				if (session.getTransaction().isActive()) {
					session.getTransaction().commit();
				}
				session.close();

			}

		}
		return roleWiseUser;
	}
	
	public List<RoleWiseUser> getRoleWiseUserList() {
		return commonService.getDataList(new RoleWiseUser(),null);
	}


}
