package edu.du.obss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import edu.du.obss.util.HibernateUtil;

@SpringBootApplication
@ImportResource("classpath:spring-beans.xml")
@ComponentScan("edu.du.*")
public class ObssServerApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	
	    return application.sources(ObssServerApplication.class);
	}
	public static void main(String[] args) {
//		SpringApplication.run(ObssServerApplication.class, args);
		 try(Session session = HibernateUtil.getSessionFactory().openSession()) {
				// Check MySQL database version
				String sql = "select count(*) from V$SESSION_CONNECT_INFO";
				int result =  session.createNativeQuery(sql).getFirstResult();
				System.out.println(result);
			} catch (HibernateException e) {
				e.printStackTrace();
			}
	}

}



