package edu.du.obss.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.Books;
import edu.du.obss.model.Review;
import edu.du.obss.service.ReviewService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin(origins="*")
public class ReviewController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private ReviewService reviewService;
	@Value("${file.store.location}")
	private String fileStoreLocation;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/review", method = RequestMethod.POST)
	@ResponseBody
	public String bookInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Review review = new Review();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Review> reviewList = new ArrayList<Review>();
		reviewList = (List<Review>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Review>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				review = reviewService.saveOrUpdateReview(reviewList.get(0));
				
				log.info("Review Saved.");
				reviewList.set(0, review);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				reviewList = reviewService.getReviewList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					review = reviewService.saveOrUpdateReview(reviewList.get(0));
					
					log.info("Review Updated.");
					reviewList.set(0, review);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						review = reviewService.DeleteReview(reviewList.get(0));
						
						log.info("Review Deleted.");
						reviewList.set(0, review);
							
						}

			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(reviewList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	@GetMapping("/showReviewForUser/{user}")
	public String getAllItem(@PathVariable String user) throws Exception {
		List<Review> employeeObj =  reviewService.showReviewByUser(Integer.parseInt(user));
		if(employeeObj == null) {
			throw new RuntimeException("review of id: "+user+" is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}
}
