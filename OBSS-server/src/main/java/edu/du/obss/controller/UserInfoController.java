package edu.du.obss.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
//import com.sbl.spms.model.Contractbillentry;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.Books;
import edu.du.obss.model.UserInfo;
import edu.du.obss.service.MailSendService;
import edu.du.obss.service.UserInfoService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;



@RestController
@Slf4j
@CrossOrigin(origins="*")
public class UserInfoController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	@Autowired
	private UserInfoService userInfoService;
	 @Autowired
	    private MailSendService mailSendService;
	/*@Autowired
	private VendorInfoService vendorInfoService;*/
	@Value("${file.store.location}")
	private String fileStoreLocation;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String handleLoginRequest(@RequestBody String  encodedMessageString) throws Exception {
		log.info("User Login Request Accepted. Request : {}" , encodedMessageString);
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		
		/*
		 * TO_DO:H:Need to do Message Validation
		 */
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		DataHeader messageHeader = message.getDataHeader();
		
		try {
			if(messageHeader.getActionType().equals(ActionTypes.USER_LOGIN.toString())) {
				userInfoList = (List<UserInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<UserInfo>>(){}.getType());
				userInfoList.set(0, userInfoService.login(userInfoList.get(0).getUserId(),userInfoList.get(0).getPassword()));
			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
			
		}
		catch (Exception e) {
			log.error(e.getLocalizedMessage());
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			userInfoList=null;
			
		}
		if(messageHeader.getActionType().equals(ActionTypes.USER_LOGIN.toString())) {
			message.setPayLoad(userInfoList);
		}
		
		byte[] encoded = Base64.getEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
		
		
		return new String(encoded);
		//return messageProcessor.getResponseAsJson(message);
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/userInfo", method = RequestMethod.POST)
	@ResponseBody
	public String insertUserInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		UserInfo userInfo = new UserInfo();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		List<UserInfo> adminList = new ArrayList<UserInfo>();
		userInfoList = (List<UserInfo>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<UserInfo>>(){}.getType());
		log.info("userInfoList " , userInfoList);
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.USER_REGISTER.toString())) {
				userInfo = userInfoService.saveUserInfo(userInfoList.get(0));
				
				log.info("User Info Saved.");
				
				adminList = userInfoService.showAllAdmins();
				for (UserInfo admin : adminList) {
					mailSendService.sendMailForRegistration(admin);
				}
				
				
				userInfoList.set(0, userInfo);
				
			}
			
			else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
				
				userInfo = userInfoService.updateUser(userInfoList.get(0));
					log.info("User Info Updated.");
					userInfoList.set(0, userInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
					
				userInfo = userInfoService.deleteAllUserInfo(userInfoList.get(0));
				 log.info("User Info Deleted.");
				 userInfoList.set(0, userInfo);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				int approved = 1;
				userInfoList =  userInfoService.userList(approved);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.REJECT_USERS.toString())) {
				userInfoList = userInfoService.reject(userInfoList);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.SELECT_APPROVAL_REQUEST_LIST.toString())) {
				int approved = 0;
				userInfoList = userInfoService.userList(approved);
					
				}
			else if(messageHeader.getActionType().equals(ActionTypes.APPROVE_USERS.toString())) {
				
				userInfoList = userInfoService.approve(userInfoList);
					
				}
			
			
		
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(userInfoList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	/*@GetMapping("/matchUserId/{userId}")
	public boolean getAllBrach(@PathVariable String userId) {
		return vendorInfoService.matchUserId(userId);
	}*/
	
	
//	@SuppressWarnings("null")
	@RequestMapping("/userWithImage")
	public List<String> uploadUserWithImage(@RequestParam("file") MultipartFile file,@RequestParam(name="user") String jsonUser, HttpServletRequest request) throws IOException {
    	
    	UserInfo user = (UserInfo) messageProcessor.getObjectFromJsonString(jsonUser, new TypeToken<UserInfo>() {}.getType());
    	UserInfo userAfter = new UserInfo();
		 Part filePart = null;
		 File fileStore = null;
		 String message = null;
		 String messageHeader = null;
		 List<String> messageList = new ArrayList<>();
		try {
			if(user.getId()!=null) {
				user.setImg_url(user.getId()+"."+getFileExtension(file.getOriginalFilename()));
			}
			userAfter = userInfoService.saveOrUpdateUserInfo(user);
			if(userAfter!=null  ) {
				if(file.getOriginalFilename() !=null) {
				fileStore = new File(fileStoreLocation+"/User/"+ user.getId().toString()+"."+getFileExtension(file.getOriginalFilename()));
				
				FileUtils.writeByteArrayToFile(fileStore, file.getBytes());	
//				userAfter.setImg_url(user.getId().toString()+"."+getFileExtension(file.getOriginalFilename()));
//				userInfoService.saveOrUpdateUserInfo(userAfter);
			}
				
				messageHeader="success";
				message="Profile Updated Successfully";
			}
		
		
			
		} catch (Exception e) {
			e.printStackTrace();
			messageHeader="error";
			message=e.getMessage();
		} 
			
		   
		    messageList.add(messageHeader);
		    messageList.add(message);
			return messageList;

    }
	
	
	@GetMapping("/userList")
	public String getUsers() throws Exception{
		 ObjectMapper objectMapper = new ObjectMapper();
		 List<UserInfo> usrlist =  userInfoService.getLabels();
		 String usr =objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(usrlist);
		 return usr;
	}
	
	@GetMapping("/showDetailByUser/{user}")
	public String getAllItem(@PathVariable String user) throws Exception {
		UserInfo employeeObj =  userInfoService.showDetailByUser(Integer.parseInt(user));
		if(employeeObj == null) {
			throw new RuntimeException("User with id: "+user+" is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}
	
	private static String getFileExtension(String fileName) {
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
}
