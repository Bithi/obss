package edu.du.obss.controller;


import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.Books;
import edu.du.obss.model.Role;
import edu.du.obss.model.UserInfo;
import edu.du.obss.model.Writer;
import edu.du.obss.service.BookService;
import edu.du.obss.service.RoleInfoService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

import edu.du.obss.service.WriterService;
import edu.du.obss.util.DataProcessor;
@RestController
@Slf4j
@CrossOrigin(origins="*")
public class WriterController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private WriterService writerService;
	
	@Autowired
	private BookService bookService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/writer", method = RequestMethod.POST)
	@ResponseBody
	public String writerInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Writer writer = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Writer> writerList = new ArrayList<Writer>();
		writerList = (List<Writer>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Writer>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				writer = writerService.saveOrUpdateWriter(writerList.get(0));
				
				log.info("Writer Info Saved.");
				writerList.set(0, writer);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				writerList = writerService.getWriterList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					writer = writerService.updateWriterInfo(writerList.get(0));
						log.info("Writer Updated.");
						writerList.set(0, writer);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						writer = writerService.DeleteWriterInfo(writerList.get(0));
						 log.info("Writer Deleted.");
						 writerList.set(0, writer);
							
						}

			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(writerList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	
	
	@GetMapping("/writerList")
	public List<Writer> getWriterlist() throws Exception{
		List<Writer> writerList= writerService.getLabels();
		return writerList;
	}
	
	
}
