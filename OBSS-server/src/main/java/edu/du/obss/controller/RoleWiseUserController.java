package edu.du.obss.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.RoleWiseUser;
import edu.du.obss.service.RoleInfoService;
import edu.du.obss.service.RoleWiseUserService;
import edu.du.obss.service.UserInfoService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin(origins="*")
public class RoleWiseUserController {

	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private RoleWiseUserService roleWiseUserService;
	
	
	@Autowired
	private RoleInfoService roleInfoService;
	
	@Autowired
	private UserInfoService userInfoService;
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/roleWiseUser", method = RequestMethod.POST)
	@ResponseBody
	public String handleRoleWiseUser(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		RoleWiseUser roleWiseUser = new RoleWiseUser();
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<RoleWiseUser> roleWiseUserList = new ArrayList<RoleWiseUser>();
		roleWiseUserList = (List<RoleWiseUser>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<RoleWiseUser>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				roleWiseUser = roleWiseUserService.saveOrUpdateRoleWiseUser(roleWiseUserList.get(0));
				
				log.info("Role Wise User Saved.");
				roleWiseUserList.set(0, roleWiseUser);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				roleWiseUserList = roleWiseUserService.getRoleWiseUserList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					roleWiseUser = roleWiseUserService.updateRoleWiseUserInfo(roleWiseUserList.get(0));
						log.info("Role Wise User  Updated.");
						roleWiseUserList.set(0, roleWiseUser);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						roleWiseUser = roleWiseUserService.DeleteRoleWiseUser(roleWiseUserList.get(0));
						 log.info("Menu Info Deleted.");
						 roleWiseUserList.set(0, roleWiseUser);
					}
							
			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(roleWiseUserList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
//	@GetMapping("/userList")
//	public List<UserInfo> getUserlist() throws Exception{
//		return userInfoService.getUserInfoList();
//	}
	
//	@GetMapping("/roleList")
//	public List<Role> getRoles() throws Exception{
//		return roleInfoService.getLabels();
//	}
//	

}
