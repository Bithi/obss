package edu.du.obss.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.Category;
import edu.du.obss.model.Writer;
import edu.du.obss.service.CategoryService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin(origins="*")
public class CategoryController {
	@Autowired
	private DataProcessor<?> messageProcessor;
	
	@Autowired
	private CategoryService categoryService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/category", method = RequestMethod.POST)
	@ResponseBody
	public String categoryInfo(@RequestBody String  encodedMessageString) throws Exception {
		
		log.info("Request Accepted. Request : {}" , encodedMessageString);
		
		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Category category = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString,new TypeToken<Data>(){}.getType());
		
		List<Category> categoryList = new ArrayList<Category>();
		categoryList = (List<Category>) messageProcessor.getObjectListFromPayload(message.getPayLoad(), new TypeToken<List<Category>>(){}.getType());
		DataHeader messageHeader = message.getDataHeader();
		
		
		try {
			
			if(messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				category = categoryService.saveOrUpdateCategory(categoryList.get(0));
				
				log.info("category Info Saved.");
				categoryList.set(0, category);
				
			}else if(messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {
				
				categoryList = categoryService.getCategoryList();
					
				}else if(messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {
					
					category = categoryService.updateCategory(categoryList.get(0));
						log.info("category Updated.");
						categoryList.set(0, category);
						
					}else if(messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
						
						category = categoryService.DeleteCategoryInfo(categoryList.get(0));
						 log.info("category Deleted.");
						 categoryList.set(0, category);
							
						}

			
						
	    	messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(), StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());
	    	
			
		}catch (Exception e) {
				e.printStackTrace();
				messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(), "404", e.getMessage());
			}
		
		    message.setPayLoad(categoryList);
			byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());
			
			return new String(encoded);
	}
	
	@GetMapping("/categoryList")
	public List<Category> getCatlist() throws Exception{
		List<Category> catList= categoryService.getLabels();
		return catList;
	}
	
}
