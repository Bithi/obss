package edu.du.obss.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.du.obss.constants.ActionTypes;
import edu.du.obss.constants.Constant;
import edu.du.obss.constants.StatusType;
import edu.du.obss.model.Books;
import edu.du.obss.model.Review;
import edu.du.obss.model.TrBook;
import edu.du.obss.model.UserInfo;
import edu.du.obss.model.Writer;
import edu.du.obss.model.WriterBook;
import edu.du.obss.service.BookService;
import edu.du.obss.service.WriterBookService;
import edu.du.obss.service.WriterService;
import edu.du.obss.util.Data;
import edu.du.obss.util.DataHeader;
import edu.du.obss.util.DataProcessor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class BookController {
	@Autowired
	private DataProcessor<?> messageProcessor;

	@Autowired
	private WriterService writerService;

	@Autowired
	private BookService bookService;

	@Autowired
	private WriterBookService writerBookService;

	@Value("${file.store.location}")
	private String fileStoreLocation;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@ResponseBody
	public String bookInfo(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		Books book = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<Books> bookList = new ArrayList<Books>();
		bookList = (List<Books>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<Books>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();
		WriterBook writerBook = new WriterBook();
		WriterBook writerBookDel = new WriterBook();
		List<WriterBook> writerBookList = new ArrayList<WriterBook>();

		try {

			if (messageHeader.getActionType().equals(ActionTypes.SAVE.toString())) {
				book = bookService.saveOrUpdateBook(bookList.get(0));

				log.info("Book Info Saved.");
				bookList.set(0, book);

			} else if (messageHeader.getActionType().equals(ActionTypes.SELECT_ALL.toString())) {

				bookList = bookService.getBookList();

			} else if (messageHeader.getActionType().equals(ActionTypes.UPDATE.toString())) {

				book = bookList.get(0);

				if (book.getWriter() != null) {

					writerBook.setBook(book.getId());
					writerBook.setWriter(Integer.parseInt(book.getWriter()));
					writerBookList = writerBookService.showWriterBookByBook(writerBook);

					for (int i = 0; i <= writerBookList.size(); i++) {
						writerBookDel = new WriterBook();
						writerBookDel = writerBookList.get(i);
						writerBookService.DeleteWriterBook(writerBookDel);
					}

					writerBookService.saveOrUpdateWriter(writerBook);
				}
				book = bookService.updateBook(book);
				log.info("Book Updated.");
				bookList.set(0, book);

			} else if (messageHeader.getActionType().equals(ActionTypes.DELETE.toString())) {
				Books bookForDel = new Books();
				bookForDel = bookList.get(0);
				if (bookForDel.getWriter() != null) {

					writerBook.setBook(bookForDel.getId());
					writerBook.setWriter(Integer.parseInt(bookForDel.getWriter()));
					writerBookList = writerBookService.showWriterBookByBook(writerBook);

					for (int i = 0; i <= writerBookList.size(); i++) {
						writerBookDel = new WriterBook();
						writerBookDel = writerBookList.get(i);
						writerBookService.DeleteWriterBook(writerBookDel);
					}
					book = bookService.DeleteBookInfo(bookList.get(0));
					log.info("Book Deleted.");
					bookList.set(0, book);

				}
			
			}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(bookList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/trBook", method = RequestMethod.POST)
	@ResponseBody
	public String trBook(@RequestBody String encodedMessageString) throws Exception {

		log.info("Request Accepted. Request : {}", encodedMessageString);

		byte[] decoded = Base64.getMimeDecoder().decode(encodedMessageString);
		String messageString = new String(decoded);
		TrBook trBook = null;
		Data message = (Data) messageProcessor.getObjectFromJsonString(messageString, new TypeToken<Data>() {
		}.getType());

		List<TrBook> trBookList = new ArrayList<TrBook>();
		trBookList = (List<TrBook>) messageProcessor.getObjectListFromPayload(message.getPayLoad(),
				new TypeToken<List<TrBook>>() {
				}.getType());
		DataHeader messageHeader = message.getDataHeader();

		try {

			 if (messageHeader.getActionType().equals(ActionTypes.REQUEST.toString())) {
				trBook = bookService.trBook(trBookList.get(0));

				log.info("Book Requested.");
				trBookList.set(0, trBook);

			}
			 
			 else if (messageHeader.getActionType().equals(ActionTypes.APPROVE.toString())) {
					trBook = bookService.trBook(trBookList.get(0));

					log.info("Book Approved.");
					trBookList.set(0, trBook);

				}
			 
			 else if (messageHeader.getActionType().equals(ActionTypes.REJECT.toString())) {
					trBook = bookService.trBook(trBookList.get(0));

					log.info("Book Rejected.");
					trBookList.set(0, trBook);

				}
			 
			 else if (messageHeader.getActionType().equals(ActionTypes.RETURN.toString())) {
					trBook = bookService.trBook(trBookList.get(0));

					log.info("Book Approved.");
					trBookList.set(0, trBook);

				}
			 
			 else if (messageHeader.getActionType().equals(ActionTypes.CLAIM.toString())) {
					trBook = bookService.trBook(trBookList.get(0));

					log.info("Book Claimed.");
					trBookList.set(0, trBook);

				}

			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_SUCCESS.toString(),
					StatusType.STATUS_CODE_200.toString(), Constant.STR_EMPTY.toString());

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = messageProcessor.buildMessageHeader(messageHeader, StatusType.STATUS_ERROR.toString(),
					"404", e.getMessage());
		}

		message.setPayLoad(trBookList);
		byte[] encoded = Base64.getMimeEncoder().encode(messageProcessor.getResponseAsJson(message).getBytes());

		return new String(encoded);
	}
	

	@SuppressWarnings("null")
	@RequestMapping("/bookWithImage")
	public List<String> uploadFile(@RequestParam(name = "file", required = false) MultipartFile file,
			@RequestParam(name = "book") String jsonBook, @RequestParam(name = "writerBook") String jsonWriterBook,
			HttpServletRequest request) throws IOException {

		Books book = (Books) messageProcessor.getObjectFromJsonString(jsonBook, new TypeToken<Books>() {
		}.getType());
		WriterBook writerBook = (WriterBook) messageProcessor.getObjectFromJsonString(jsonWriterBook,
				new TypeToken<WriterBook>() {
				}.getType());
		Books bookAfterInsert = new Books();
		WriterBook writerBookAfterInsert = new WriterBook();
		Part filePart = null;
		File fileStore = null;
		String message = null;
		String messageHeader = null;
		List<String> messageList = new ArrayList<>();
		try {
			if (book.getId() != null) {
				book.setImgUrl(book.getId() + "." + getFileExtension(file.getOriginalFilename()));
			}

			bookAfterInsert = bookService.saveOrUpdateBook(book);
			if (bookAfterInsert != null) {
				if (file.getOriginalFilename() != null) {
					fileStore = new File(fileStoreLocation + "/User/" + bookAfterInsert.getCreatedBy() + "/library/",
							bookAfterInsert.getId().toString() + "." + getFileExtension(file.getOriginalFilename()));
					FileUtils.writeByteArrayToFile(fileStore, file.getBytes());
					// book.setImgUrl(bookAfterInsert.getId().toString()+"."+getFileExtension(file.getOriginalFilename()));
					// bookService.updateBook(book);
				}
				if (writerBook.getWriter() != null) {
					writerBook.setBook(bookAfterInsert.getId());
					writerBookAfterInsert = writerBookService.saveOrUpdateWriter(writerBook);
				}
				messageHeader = "success";
				message = "Book saved Successfully";
			}

		} catch (Exception e) {
			e.printStackTrace();
			messageHeader = "error";
			message = e.getMessage();
		}

		messageList.add(messageHeader);
		messageList.add(message);
		return messageList;

	}

	private static String getFileExtension(String fileName) {
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	@GetMapping("/showBooksByUser/{user}")
	public String getAllItem(@PathVariable String user) throws Exception {
		List<Books> employeeObj = bookService.showBooksByUser(user);
		if (employeeObj == null) {
			throw new RuntimeException("Books with id: " + user + " is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}

	@GetMapping("/showBooksDetailById/{bookId}")
	public Object getAllDetail(@PathVariable int bookId) {
		Object bookObj = null;
		try {
			bookObj = bookService.showBooksDetailById(bookId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (bookObj == null) {
			throw new RuntimeException("Books with id: " + bookId + " is not found.");
		}
		return bookObj;
	}

	@GetMapping("/showAllBooksDetail/{userId}")
	public String getAllBooksDetail(@PathVariable int userId) throws Exception {
		List<Books> employeeObj = bookService.showAllBooksDetails(userId);
		if (employeeObj == null) {
			throw new RuntimeException("Books not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}

	@GetMapping("/bookList")
	public List<Books> getBooklist() throws Exception {
		List<Books> bookList = bookService.getLabels();
		return bookList;
	}
	
//	@RequestMapping(path = "showReqBookStatus/{requster}/{book}", method = RequestMethod.GET)
	@GetMapping("/showReqBookStatus/{requster}/{book}")
	public String showReqBookStatus(@PathVariable int requster, @PathVariable int book) throws Exception {
		String status = bookService.showReqBookStatus(requster,book);
		return status;
	}
	
	@GetMapping("/requestedBookList/{owner}")
	public String getRequestedBookList(@PathVariable int owner) throws Exception {
		List<TrBook> employeeObj = bookService.showTrBooksByOwner(owner);
		if (employeeObj == null) {
			throw new RuntimeException("Books with id: " + owner + " is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}
	
	@GetMapping("/toBeReturnBookList/{req}")
	public String gettoBeReturnBookList(@PathVariable int req) throws Exception {
		List<TrBook> employeeObj = bookService.showToBeReturnByOwner(req);
		if (employeeObj == null) {
			throw new RuntimeException("Books with requester" + req + " is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}
	
	
	@GetMapping("/getToClaimBooks/{owner}")
	public String getToClaimBooksList(@PathVariable int owner) throws Exception {
		List<TrBook> employeeObj = bookService.showToClaimBooks(owner);
		if (employeeObj == null) {
			throw new RuntimeException("Books with requester" + owner + " is not found.");
		}
		String json = new Gson().toJson(employeeObj);
		return json;
	}
	
	
}
